import axios from 'axios';
import { SHOW_MESSAGE } from '../systemMsg';

export function editProtetico ({ id, props, message, update }, callback) {
	return async dispatch => {
		console.log(id)
		try {
			const res = await axios.put('/api/proteticos/'+id, props);
			console.log(res)
			if (update)
				dispatch({ type: SHOW_MESSAGE, payload: { message: message?message:`Protetico editado com sucesso`, type: 'success', undo: { db: 'protetico', props: update } } });
			else
				dispatch({ type: SHOW_MESSAGE, payload: { message: message?message:`Protetico editado com sucesso`, type: 'success' } });
			callback(res.data);
		} catch(err) {
			console.log(err);
		}
	}
};