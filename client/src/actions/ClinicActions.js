import axios from 'axios';

export const PAGINATE_CLINICS = 'PAGINATE_CLINICS';


export function paginationClinics({ active }, callback) {
	return async dispatch => {
		try {
            const res = await axios.get('/api/listClinics');
			dispatch({ type: PAGINATE_CLINICS, payload: res.data, status: active });
			callback();
		} catch(err) {
			console.log(err);
		}
	}
};