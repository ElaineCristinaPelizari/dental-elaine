import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export default function verifyAuth(ComposedComponent) {
	const propTypes = {
		/**  The text of the Button */
		history: PropTypes.object.isRequired
	}

	class VerifyAuth extends Component {

		componentWillReceiveProps(){
			if (this.props.auth.authenticated) {
				this.props.history.push('/dashboard');
			}
		}

		componentWillMount(){
			if (this.props.auth.authenticated) {
				this.props.history.push('/dashboard');
			}
		}

		componentDidMount() {
			if (this.props.auth.authenticated) {
				this.props.history.push('/dashboard');
			}
		}

		componentDidUpdate() {
			if (!this.props.auth.authenticated) {
				this.props.history.push('/dashboard');
			}
		}

		render() {
			return <ComposedComponent {...this.props} />
		}
	}

	VerifyAuth.propTypes = propTypes;

	const composedComponentName = ComposedComponent.displayName
		|| ComposedComponent.name
		|| 'Component';

	VerifyAuth.displayName = `verifyAuth(${composedComponentName})`;

	function mapStateToProps(state) {
		return { auth: state.auth };
	}

	return connect(mapStateToProps)(VerifyAuth);
}
