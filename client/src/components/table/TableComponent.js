import React from 'react';
import MUIDataTable from 'mui-datatables';
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Add from "@material-ui/icons/Add";

export default function MaterialTableDemo( props ) {

  const { titulo, colunas, objetos, inserir, funcoes, selecionarLinhas } = props;
  
  const options = {
    filter: true,
    selectableRows: selecionarLinhas?selecionarLinhas:'multiple',
    filterType: 'dropdown',
    responsive: 'stacked',
    rowsPerPage: 10,
    onRowsDelete: (rowsDeleted) => {
      if(funcoes !== undefined){
        var deletar = [];
        rowsDeleted.data.map(d => {
  
          var objeto = objetos.filter((obj, index) => {
            return index === d.dataIndex;
          })
          deletar.push(objeto);
        })
        funcoes.deletar(deletar);
      }
      
    },
    customToolbar: () => {
      return (
        !inserir ?
          ""
        :
        <Tooltip title={"Inserir"}>
          <IconButton onClick={() => props.history.push(inserir.link)}>
            <Add/>
          </IconButton>
        </Tooltip>
      )
    }
  }

  return (
    <MUIDataTable
        title={titulo}
        columns={colunas}
        data={objetos}
        options={options}
    />
  );
}
