import React, { Component } from 'react';
import { connect } from 'react-redux';

import { css } from 'aphrodite/no-important';
import { styles } from '../forms/InputFieldStyles';

import Button from '../common/Button';

import { listarTodoStatus } from '../../api/pacientes';

class StatusLista extends Component {
    constructor(props) {
        super(props);

		this.addStatus = this.addStatus.bind(this);
		this.removeStatus = this.removeStatus.bind(this);
		this.handleChange = this.handleChange.bind(this);

		this.state = {
			// status: [{ name: '', value: '' }],
			status:[{status_id:'', item_status_id:''}],
			listaStatus:'',
			StatusSelecionado:'',
			StatusItemSelecionado:''
		};
	}

	async componentWillMount(){
		var objStatus = await listarTodoStatus(this.props.clinica._id);
		this.setState({listaStatus:objStatus})
	}

	componentWillReceiveProps(nextProps) {
        this.setState({ status: [...nextProps.status] });
	}
	
	handleChange(event) {
		switch(event.target.id){
			case "status":
				this.setState({StatusSelecionado: event.target.value, StatusItemSelecionado:''});
				break;
			case "statusItem":
				this.setState({StatusItemSelecionado: event.target.value});
			default:
				break;
		}
	}

	addStatus = () => {

		const { getStatus } = this.props;
        var { StatusSelecionado, StatusItemSelecionado } = this.state;


		if(StatusItemSelecionado !== '' ){
			var existe = this.state.status.filter(function(statusAdd){return statusAdd.item_status_id === StatusItemSelecionado})[0];
			if(!existe){
				var select = this.state.listaStatus.filter((status) =>  {
					return status.status_id === StatusSelecionado
				})[0].itens.filter((item) => {
					return item.item_status_id === StatusItemSelecionado; 
				})[0];

				this.setState({
					status: this.state.status.concat([{status_id: select.status_id, item_status_id: select.item_status_id}])
                });
		
				getStatus(this.state.status.concat([{status_id: select.status_id, item_status_id: select.item_status_id}]));
			}

		}
	}
	
	removeStatus = (idx) => () => {

		const { getStatus } = this.props;
		this.setState({
			status: this.state.status.filter((s, sidx) => idx !== sidx )
		});

		getStatus(this.state.status.filter((s, sidx) => idx !== sidx));
	}
	
	render() {
		const { listaStatus } = this.state;

		return (
			<div>
				{listaStatus &&
                <select id="status" className={css(styles.inputSelect)} value={this.state.StatusSelecionado} defaultValue={0} onChange={this.handleChange}>
					<option value="">Selecione ---</option>
					{listaStatus.map((status, index) => (
						<option value={status.status_id} key={index}>{status.status}</option>
					))}
                </select>
				}
				{this.state.StatusSelecionado &&
				<select id="statusItem" className={css(styles.inputSelect)} value={this.state.StatusItemSelecionado} defaultValue={0} onChange={this.handleChange}>
					<option value="">Selecione ---</option>
					{listaStatus.filter((status) => { return status.status_id === this.state.StatusSelecionado})[0].itens.map((status, index) => (
						<option value={status.item_status_id} key={index}>{status.item_titulo}</option>
					))}
				</select>
				}
                <Button
					text='Adicionar'
					color="primary"
					onClick={this.addStatus}
				/>
				{listaStatus &&
				this.state.status.map( (status, idx) => (
					<div key={idx} className={css(styles.table)} >

						<div className={css(styles.fieldset, styles.w40)}>
						{ idx == 0 ? <label className={css(styles.label)}>Status</label> : ''}
							<label className={css(styles.fieldset)}>
								{
									status.status_id &&
									listaStatus.filter((st) =>  {
										return st.status_id === status.status_id
									})[0].itens.filter((item) => {
										return item.item_status_id === status.item_status_id; 
									})[0].status
								}
							</label>
						</div>

						<div className={css(styles.fieldset, styles.w40)}>
						{ idx == 0 ? <label className={css(styles.label)}>Item</label> : ''}
							<label className={css(styles.fieldset)}>
								{
									status.status_id &&
									listaStatus.filter((st) =>  {
										return st.status_id === status.status_id
									})[0].itens.filter((item) => {
										return item.item_status_id === status.item_status_id; 
									})[0].item_titulo
								}
							</label>
						</div>
						
                        <div className={css(styles.fieldset, styles.w20)}>
                            <span className={css(styles.link, styles.red)} onClick={this.removeStatus(idx)}>Remover</span>
                        </div>
					</div>
				)) }
			</div>
		)
	}

}

function mapStateToProps(state) {
	const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}

export default connect(mapStateToProps)(StatusLista);
