import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import { css } from 'aphrodite/no-important';
import { styles } from './PatientFormStyles';

import { createPatientFull, getPatient, clearPatient, updatePatient } from '../../../actions/patientsCreation';

import Button from '../../common/Button';
import InputField from '../../forms/InputField';
import DateTimePicker from '../date/DateTimePicker';
import TelephoneList from '../../lists/TelephoneList';
import SituacaoLista from '../../lists/SituacaoLista';
import StatusLista from '../../lists/StatusLista';
import ObservacaoLista from '../../lists/ObservacaoLista';
import { buscarCep } from '../../../api/Cep';

const PATIENT = {
	name: { name: 'name', label: 'Nome', placeholder: 'Nome do(a) paciente' },
	lastname: { name: 'lastname', label: 'Sobrenome', placeholder: 'Sobrenome do(a) paciente' },
	email: { name: 'email', label: 'E-mail', placeholder: 'paciente@email.com' },
	registry: { name: 'registry', label: 'Número de Registro', placeholder: 'XXXXXXXX' },

	telNumber: { name: 'telNumber', label: 'Telefone', placeholder: '(XX) XXXXX XXXX', mask: '(99) 99999 9999' },
	telType: { name: 'telType', label: 'Tipo', placeholder: 'Ex.: Celular, Principal...' },

	zip: { name: 'zip', label: 'CEP', placeholder: 'XXXXX - XXX' },
	address: { name: 'address', label: 'Endereço', placeholder: 'Ex.: Av. Brasil, 999' },
	numero: { name: 'numero', label: 'Nº', placeholder: 'Ex.: 93, 101...' },
	complemento: { name: 'complemento', label: 'Complemento', placeholder: 'Ex.: Casa, Apto...' },
	bairro: { name: 'bairro', label: 'Bairro', placeholder: 'Ex.: Centro...' },
	state: { name: 'state', label: 'Estado', placeholder: 'Ex.: Paraná, São Paulo...' },
	city: { name: 'city', label: 'Cidade', placeholder: 'Ex.: Curitiba, São Paulo...' },

	situacoes: { name: 'situacoes', label: 'Situações', type: 'select' },
	status: { name: 'status', label: 'Status', type: 'select' },
	observacoes: { name: 'observacoes', label: 'Observações', type: 'text' },

	profession: { name: 'profession', label: 'Profissão', placeholder: 'Profissão' },
	company: { name: 'company', label: 'Empresa', placeholder: 'Empresa' },
	rg: { name: 'civil_id', label: 'RG', placeholder: '000.000.000 - 0' },
	cpf: { name: 'cpf', label: 'CPF', placeholder: '000.000.000 - 00', mask: '999.999.999 - 99' },
	father: { name: 'father', label: 'Nome do pai', placeholder: 'Nome do pai' },
	mother: { name: 'mother', label: 'Nome da mãe', placeholder: 'Nome da mãe' },
	father_profession: { name: 'father_profession', label: 'Profissão do pai', placeholder: 'Profissão do pai' },
	mother_profession: { name: 'mother_profession', label: 'Profissão da mãe', placeholder: 'Profissão da mãe' },
	insurance: { name: 'insurance', label: 'Plano Odontológico', placeholder: 'Plano Odontológico' },
	insurance_number: { name: 'insurance_number', label: 'Número Plano Odontológico', placeholder: 'XXX.XXX.XXX.XXX' },
	sponsor: { name: 'sponsor', label: 'Responsável', placeholder: 'Nome do responsável' },
	sponsor_insurance_number: { name: 'sponsor_insurance_number', label: 'Número do titular do plano odontológico', placeholder: 'XXX.XXX.XXX.XXX' },
	sponsor_cpf: { name: 'sponsor_cpf', label: 'CPF do responsável', placeholder: '000.000.000 - 00', mask: '999.999.999 - 99' },
	indication: { name: 'indication', label: 'Indicação', placeholder: 'Indicação' },
};

// PatientForm handles the form where the user enter the app
class PatientForm extends Component {
	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
		this.onBirthChange = this.onBirthChange.bind(this);
		this.renderFields = this.renderFields.bind(this);
		this.renderForm = this.renderForm.bind(this);
		this.getTelephones = this.getTelephones.bind(this);
		this.getSituacoes = this.getSituacoes.bind(this);
		this.getStatus = this.getStatus.bind(this);
		this.getObservacoes = this.getObservacoes.bind(this);
		this.getTelError = this.getTelError.bind(this);
		this.handleDados = this.handleDados.bind(this);
		this.onPrimeiraConsulta = this.onPrimeiraConsulta.bind(this);
		this.onRetornoConsulta = this.onRetornoConsulta.bind(this);

		this.state = {
			birth: null,
			consulta: null,
			retorno: null,
			telephones: [],
			telError: null,
			situacoes: [],
			status: [],
			observacoes: [],
			address: '',
			city: '',
			state: '',
			bairro: '',
		}
	}

	handleDados(zip) {
		if (zip.target.value.length == 8) {
			const c = zip.target.value;
			buscarCep(c).then((res) => {
				console.log(res);
				if (res != null) {
					this.setState({
						address: res.logradouro,
						city: res.cidade,
						state: res.estado,
						bairro: res.bairro
					})
				} else {
					return false;
				}
			})
		}
	}

	componentDidMount() {
		const { selectedPatient } = this.props;

		if (selectedPatient) {
			this.setState({
				birth: selectedPatient.birthday,
				telephones: selectedPatient.telephones,
				situacoes: selectedPatient.situacoes,
				status: selectedPatient.status,
				observacoes: selectedPatient.observacoes,
				consulta: selectedPatient.consulta1,
				retorno: selectedPatient.retornoP
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		const { selectedPatient } = this.props;

		if (selectedPatient && selectedPatient._id !== nextProps.selectedPatient._id) {
			this.setState({
				birth: nextProps.selectedPatient.birthday,
				telephones: nextProps.selectedPatient.telephones,
				situacoes: nextProps.selectedPatient.situacoes,
				status: nextProps.selectedPatient.status,
				observacoes: nextProps.selectedPatient.observacoes,
				consulta: nextProps.selectedPatient.consulta1,
				retorno: nextProps.selectedPatient.retornoP
			});
		} else if (selectedPatient) {
			this.setState({
				birth: selectedPatient.birthday,
				telephones: selectedPatient.telephones,
				situacoes: selectedPatient.situacoes,
				status: selectedPatient.status,
				observacoes: selectedPatient.observacoes,
				consulta: selectedPatient.consulta1,
				retorno: selectedPatient.retornoP
			});
		}
	}

	onBirthChange(birth) {
		this.setState({
			birth: birth
		})
	}

	onPrimeiraConsulta(consulta) {
		this.setState({
			consulta: consulta
		})
	}

	onRetornoConsulta(retorno) {
		this.setState({
			retorno: retorno
		})
	}

	getTelephones(tel) {
		this.state.telephones = tel;
	}

	getSituacoes(situacao) {
		this.state.situacoes = situacao;
	}

	getStatus(tStatus) {
		this.state.status = tStatus;
	}

	getObservacoes(observacao) {
		this.state.observacoes = observacao;
	}

	getTelError(telError) {
		this.state.telError = telError;
	}

	onSubmit(values) {
		const { createPatientFull, updatePatient, selectedPatient, clinic, match, history } = this.props;

		values.clinic_id = clinic._id;

		values.telephones = this.state.telephones.filter(tel => {
			if (tel.value != '') {
				return true;
			}
			return false;
		});

		values.situacoes = this.state.situacoes.filter(situacao => {
			if (situacao.value != '') {
				return true;
			}
			return false;
		});

		values.status = this.state.status.filter(tStatus => {
			if (tStatus.value != '') {
				return true;
			}
			return false;
		});

		values.observacoes = this.state.observacoes.filter(observacao => {
			if (observacao.value != '') {
				return true;
			}
			return false;
		});

		if (this.state.birth) { values.birthday = this.state.birth; }

		if (this.state.consulta) { values.consulta1 = this.state.consulta; }

		if (this.state.retorno) { values.retornoP = this.state.retorno; }

		if (!this.state.telError) {
			if (match.params.patientId === 'registration') {
				createPatientFull(values, ret => {
					history.push(`/patients/profile/${ret._id}`);
				});
			} else {
				updatePatient(values, selectedPatient._id, ret => {
				});
			}
		}
	}

	renderFields(fields, props) {
		// iterate through the FIELDS array and create a Field for each item
		return fields.map(field => {
			return (
				<Field
					key={field.name}
					type={(field.type) ? field.type : 'text'}
					name={field.name}
					label={field.label}
					placeholder={(field.placeholder) ? field.placeholder : ''}
					mask={(field.mask) ? field.mask : ''}
					listSelect={props && props}
					component={InputField}
					defaultValue={props}
				/>
			);
		});
	}

	renderForm() {
		const { match, handleSubmit } = this.props;
		let btnText = 'Atualizar Paciente';

		if (match.params.patientId === 'registration') {
			btnText = 'Criar Paciente';
		}

		if (this.state.address !== undefined && this.state.address !== '')
			var address = this.state.address;

		if (this.state.state !== undefined && this.state.state !== '')
			var state = this.state.state;

		if (this.state.city !== undefined && this.state.city !== '')
			var city = this.state.city;

		if (this.state.bairro !== undefined && this.state.bairro !== '')
			var bairro = this.state.bairro;

		return (
			<form className={css(styles.form)} onSubmit={handleSubmit(this.onSubmit)}>
				{/* --- INFORMAÇÕES BáSICAS --- */}
				<h3 className={css(styles.sectionTitle)}>Informações básicas</h3>
				<div className={css(styles.section)}>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.registry])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.name])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.lastname])}
					</div>
				</div>

				{/* Situacoes */}
				<h3 className={css(styles.sectionTitle)}>Situações</h3>
				<div className={css(styles.section)}>
					<SituacaoLista situacoes={this.state.situacoes} getSituacoes={this.getSituacoes} />
				</div>

				{/* Status */}
				<h3 className={css(styles.sectionTitle)}>Status</h3>
				<div className={css(styles.section)}>
					<StatusLista status={this.state.status} getStatus={this.getStatus} />
				</div>

				{/* --- CONTATO --- */}
				<h3 className={css(styles.sectionTitle)}>Contato</h3>
				<div className={css(styles.section)}>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.email])}
					</div>
					<TelephoneList telephones={this.state.telephones} getTelephones={this.getTelephones} getTelError={this.getTelError} />
				</div>

				<h3 className={css(styles.sectionTitle)}>Endereço</h3>
				<div className={css(styles.section)}>
					<div className={css(styles.row_1)} onBlur={(zip) => this.handleDados(zip)}>
						{this.renderFields([PATIENT.zip])}
					</div>

					<div className={css(styles.row_1)} >
						{this.renderFields([PATIENT.address], address)}
					</div>
					<div className={css(styles.row_3)} >
						{this.renderFields([PATIENT.numero, PATIENT.complemento])}
					</div>
					<div className={css(styles.row_3)} >
						{this.renderFields([PATIENT.bairro], bairro)}
					</div>
					<div className={css(styles.row_3)} >
						{this.renderFields([PATIENT.state], state)}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.city], city)}
					</div>
				</div>

				<h3 className={css(styles.sectionTitle)}>Informações Avançadas</h3>
				<div className={css(styles.section)}>
					<div className={css(styles.row_3)}>
						{this.renderFields([PATIENT.cpf, PATIENT.rg])}
					</div>
					<div className={css(styles.row_3)}>
						<DateTimePicker name="birthday" value={this.state.birth} label="Data de Aniversário" onChange={this.onBirthChange} />
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.profession])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.company])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.mother])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.mother_profession])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.father])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.father_profession])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.sponsor])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.sponsor_cpf])}
					</div>
				</div>

				<h3 className={css(styles.sectionTitle)}>Informações do Plano Odontológico</h3>
				<div className={css(styles.section)}>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.insurance])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.insurance_number])}
					</div>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.sponsor_insurance_number])}
					</div>
				</div>

				<h3 className={css(styles.sectionTitle)}>Indicação</h3>
				<div className={css(styles.section)}>
					<div className={css(styles.row_1)}>
						{this.renderFields([PATIENT.indication])}
					</div>
				</div>

				<h3 className={css(styles.sectionTitle)}>Consulta</h3>
				<div className={css(styles.row_3)}>
					<DateTimePicker name="consulta1" value={this.state.consulta} label="Data Primeira Consulta" onChange={this.onPrimeiraConsulta} />
				</div>
				<div className={css(styles.row_3)}>
					<DateTimePicker name="retornoP" value={this.state.retorno} label="Data de Retorno" onChange={this.onRetornoConsulta} />
				</div>

				{/* Observacoes */}
				<h3 className={css(styles.sectionTitle)}>Observações</h3>
				<div className={css(styles.section)}>
					<ObservacaoLista observacoes={this.state.observacoes} getObservacoes={this.getObservacoes} />
				</div>
				<Button
					text={btnText}
					color="green"
					submit
				/>
			</form>
		);
	}

	render() {
		return (
			<div className={css(styles.flex)}>
				{this.renderForm()}
			</div>
		);
	}
}

// Redux Form function to handle form validation
function validate(values) {
	const errors = {};
	const emailRgx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	const cpfRgx = /^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{11}))$/;

	if (values.name) {
		if (values.name.trim().length <= 0) {
			errors.name = 'Qual é o nome do paciente?'
		}
	}
	if (!values.name) {
		errors.name = 'Qual é o nome do paciente?'
	}

	if (!values.email || !emailRgx.test(values.email)) {
		errors.email = 'E-mail inválido';
	}

	// if (!values.cpf || !cpfRgx.test(values.cpf)) {
	// 	errors.cpf = 'CPF inválido';
	// }

	return errors;
}

const patientForm = reduxForm({
	validate,
	enableReinitialize: true,
	form: 'patientForm'
})(PatientForm);

function mapStateToProps(state) {
	const selectedPatient = state.patientsCreation.selectedPatient;
	let initialValues = {};

	if (selectedPatient) {
		initialValues = selectedPatient;
	}

	return {
		clinic: state.auth.clinic,
		user: state.auth.user,
		selectedPatient: state.patientsCreation.selectedPatient,
		initialValues
	};
}

export default connect(mapStateToProps, { createPatientFull, getPatient, clearPatient, updatePatient })(patientForm);