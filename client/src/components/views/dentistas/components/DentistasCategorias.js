import React, { Component } from 'react';
import { connect } from 'react-redux';

import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import { cadastrarCategoria, buscarCategoria, editarCategoria } from '../../../../api/dentistas'

class DentistasCategorias extends Component {

    state = {
        formulario: [
            { type: 'text', name: 'titulo', label: 'Categoria' }
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        categoria: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.categoria_id) {
            var objCategoria = await buscarCategoria(clinica._id, this.props.match.params.categoria_id);
            this.setState({ cadastro: false, categoria: objCategoria, await: false })
        } else {
            this.setState({ await: false });
        }
    }

    editaCategoria(objeto) {
        editarCategoria(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Categoria editada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar a categoria!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))

    }

    salvarCategoria(objeto) {
        const { clinica } = this.props;

        cadastrarCategoria(clinica, objeto.titulo)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Categoria cadastrada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar a categoria!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                {this.state.alerta.abrir &&
                    <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                }
                {!this.state.await &&
                    <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.categoria} action={this.state.cadastro ? this.salvarCategoria.bind(this) : this.editaCategoria.bind(this)} />
                }
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(DentistasCategorias);