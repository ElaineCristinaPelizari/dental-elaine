import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deletarCategoria } from "../../../../api/dentistas";
import { buscarCategorias } from "../../../../api/dentistas";
import { editDentist } from '../../../../actions/dentistas/dentistSearch';
import TableComponent from '../../../table/TableComponent';
//Icone de arquivar
import EditTwoTone from '@material-ui/icons/EditTwoTone';

class DentistasConfigCategoria extends Component {
	state = {
		categorias: []
	}

	deletaCategoria(objeto) {
		const { clinica } = this.props;

		deletarCategoria(objeto, clinica._id);
	}

	componentWillMount() {

		buscarCategorias(this.props.clinica._id)
			.then(categorias => {
				var listaCategorias = []
				console.log(categorias);

				categorias.length > 0 && categorias.map((categoria, index) => {
					var novaCategoria = {};

					novaCategoria.titulo = categoria.titulo;
					novaCategoria.categoriaId = categoria._id;
					novaCategoria.funcoes = [
						<EditTwoTone key={index} onClick={() => this.props.history.push(`/dentistas/config/cadastrarCategoria/${categoria._id}`)} />
					]

					listaCategorias.push(novaCategoria);
				});

				this.setState({ categorias: listaCategorias })
			})
	}

	render() {

		const titulo = "Categorias",
			colunas = [
				{ label: 'Titulo', name: 'titulo' },
				{ label: 'Id', name: 'categoriaId' },
				{ label: '', name: 'funcoes', options: { filter: false, sort: false } },
			],
			objetos = this.state.categorias,
			inserir = {
				nome: "Cadastrar Categoria",
				link: "cadastrarCategoria"
			}

		return (
			<TableComponent funcoes={{ deletar: this.deletaCategoria.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
		);
	}
}

function mapStateToProps(state) {

	const { auth } = state;

	return {
		user: auth.user,
		clinica: auth.clinic
	};
}
export default connect(mapStateToProps, { editDentist })(DentistasConfigCategoria);