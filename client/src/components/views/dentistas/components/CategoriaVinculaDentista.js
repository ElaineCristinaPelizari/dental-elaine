import React, { Component } from 'react';
import { connect } from 'react-redux';

import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import { adicionarCategoria, buscarCategorias } from '../../../../api/dentistas';

class DentistasListagemCadastro extends Component {

    state = {
        formulario: [],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        dentista_id: '',
        categorias: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.dentista_id) {
            var objCategoria = await buscarCategorias(clinica._id);
            var formulario = this.state.formulario;

            var selectCategoria = {
                type: 'select',
                name: 'categorias',
                label: 'Categorias',
                values: []
            };

            objCategoria.map(obj => {
                var novo = {};
                novo.name = obj.titulo;
                novo.value = obj._id;
                selectCategoria.values.push(novo)
            })

            formulario.push(selectCategoria);
            this.setState({ categorias: objCategoria, await: false, dentista_id: this.props.match.params.dentista_id, formulario: formulario })

        } else {
            // this.setState({await:false});
        }
    }

    vinculaCategoria(objeto) {
        const { dentista_id } = this.state;
        adicionarCategoria(dentista_id, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Categoria vinculada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 1500)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível vincular a Categoria!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                {this.state.alerta.abrir &&
                    <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                }
                {!this.state.await &&
                    <FormComponent {...this.props} inputs={this.state.formulario} action={this.vinculaCategoria.bind(this)} />
                }
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(DentistasListagemCadastro);