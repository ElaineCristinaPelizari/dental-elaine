import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deletarEspecialidade, buscarEspecialidades } from "../../../../api/dentistas";
import { editDentist } from '../../../../actions/dentistas/dentistSearch';

import TableComponent from '../../../table/TableComponent';

//Icone de arquivar
import EditTwoTone from '@material-ui/icons/EditTwoTone';

class DentistasConfigEspecialidade extends Component {
    state = {
        especialidades: []
    }

    deletaEspecialidade(objeto) {
        const { clinica } = this.props;

        deletarEspecialidade(objeto, clinica._id);
    }

    componentWillMount() {

        buscarEspecialidades(this.props.clinica._id)
            .then(especialidades => {
                var listaEspecialidades = []
                console.log(especialidades);

                especialidades.length > 0 && especialidades.map((especialidade, index) => {
                    var novaEspecialidade = {};

                    novaEspecialidade.titulo = especialidade.titulo;
                    novaEspecialidade.especialidadeId = especialidade._id;
                    novaEspecialidade.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/dentistas/config/cadastrarEspecialidade/${especialidade._id}`)} />
                    ]
                    listaEspecialidades.push(novaEspecialidade);
                });

                this.setState({ especialidades:listaEspecialidades })
            })
    }

    render() {

        const titulo = "Especialidades",
            colunas = [
                { label: 'Titulo', name: 'titulo' },
                { label: 'Id', name: 'especialidadeId' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.especialidades,
            inserir = {
                nome: "Cadastrar Especialidade",
                link: "cadastrarEspecialidade"
            }

        return (
            <TableComponent funcoes={{ deletar:this.deletaEspecialidade.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps, { editDentist })(DentistasConfigEspecialidade);