import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { buscarDentistas, deletarDentista } from "../../../../api/dentistas";
import { editDentist } from '../../../../actions/dentistas/dentistSearch';

import TableComponent from '../../../table/TableComponent';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import PageviewTwoTone from '@material-ui/icons/PageviewTwoTone';


class DentistaListagem extends Component {
    state = {
        dentistas: []
    }

    deletaDentista(objeto) {
        const { clinica } = this.props;
        deletarDentista(objeto, clinica._id);
    }

    componentWillMount() {
        buscarDentistas(this.props.clinica._id)
            .then(dentistas => {
                var listaDentistas = []
                console.log(dentistas);
                dentistas.length > 0 && dentistas.map((dentista, index) => {
                    var novoDentista = {};
                    novoDentista.codigo = dentista.codigo;
                    novoDentista.dentistaId = dentista._id;
                    novoDentista.name = dentista.name;
                    novoDentista.email = dentista.email;
                    novoDentista.telephone = dentista.telephone;
                    novoDentista.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/dentistas/cadastrarDentista/${dentista._id}`)} />,
                        <Tooltip title={"Listar Categorias"} >
                            <IconButton onClick={() => this.props.history.push(`/dentistas/categoriaDentistaRelacao/${dentista._id}`)}>
                                <PageviewTwoTone />
                            </IconButton>
                        </Tooltip>,
                        <Tooltip title={"Listar Especialidades"}    >
                            <IconButton onClick={() => this.props.history.push(`/dentistas/especialidadeDentistaRelacao/${dentista._id}`)}>
                                <PageviewTwoTone />
                            </IconButton>
                        </Tooltip>
                    ]
                    listaDentistas.push(novoDentista);
                });
                this.setState({ dentistas: listaDentistas })
            })
    }
    render() {
        const titulo = "Dentistas",
            colunas = [
                { label: 'Id', name: 'dentistaId' },
                { label: 'Nome', name: 'name' },
                { label: 'Email', name: 'email' },
                { label: 'Telefone', name: 'telephone' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.dentistas,
            inserir = {
                nome: "Cadastrar Dentista",
                link: "cadastrarDentista"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaDentista.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps, { editDentist })(DentistaListagem);