import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import { Input } from '@material-ui/core';
import Notificacao from '../../../notificacao/Notificacao';
import { cadastrarDentista, buscarDentista, editarDentista } from '../../../../api/dentistas'
import { buscarCep } from '../../../../api/Cep';


class DentistasListagemCadastro extends Component {
    state = {
        formulario: [
            //{ type: 'text', name: 'codigo', label: 'Código' },
            { type: 'text', name: 'name', label: 'Nome' },
            { type: 'cep', name: 'cep', label: 'CEP' },
            { type: 'text', name: 'logradouro', label: 'Rua' },
            { type: 'text', name: 'numero', label: 'Nº' },
            { type: 'text', name: 'bairro', label: 'Bairro' },
            { type: 'text', name: 'cidade', label: 'Cidade' },
            { type: 'text', name: 'uf', label: 'UF' },
            { type: 'text', name: 'cpf', label: 'CPF' },
            { type: 'text', name: 'rg', label: 'RG' },
            { type: 'number', name: 'cro', label: 'CRO' },
            { type: 'number', name: 'partic', label: 'Particip.(%)' },
            { type: 'text', name: 'birthday', label: 'Data de Nasc' },
            { type: 'number', name: 'telephone', label: 'Fone' },
            { type: 'text', name: 'email', label: 'E-mail' }
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        dentista: '', 
        cadastro: true,
        await: true
    }

    // handleDados(cep) {
    //     //console.log(cep.target.value);
    //     if (cep.target.value.length == 8) {

    //         const c = cep.target.value;
    //         console.log(c);

    //         buscarCep(c).then((res) => {
    //             // Mudando o estado
    //             this.setState({
    //                 dentista:{
    //                     logradouro : res.logradouro,
    //                     bairro: res.bairro,
    //                     cidade: res.cidade,
    //                     uf: res.estado
    //                 },
    //             })
    //         })
    //     } else {
    //         return null;
    //     }
    // }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.dentista_id) {
            var objDentista = await buscarDentista(clinica._id, this.props.match.params.dentista_id);
            this.setState({ cadastro: false, dentista: objDentista, await: false })
        } else {
            this.setState({ await: false });
        }
    }

    editaDentista(objeto) {
        editarDentista(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Dentista editado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o dentista!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarDentista(objeto) {
        const { clinica } = this.props;
        cadastrarDentista(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Dentista cadastrado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o dentista!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        //console.log(this.state)
        return (
            <div>
                {this.state.alerta.abrir &&
                    <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                }
                {/* <Input type="text" name="cep" id="cep" placeholder="Cep..." onChange={(cep) => this.handleDados(cep)} /> */}
                {!this.state.await &&
                    <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.dentista} action={this.state.cadastro ? this.salvarDentista.bind(this) : this.editaDentista.bind(this)} />
                }
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(DentistasListagemCadastro);