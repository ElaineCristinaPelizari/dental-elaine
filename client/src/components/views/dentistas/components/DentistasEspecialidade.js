import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import { editarEspecialidade, buscarEspecialidade, cadastrarEspecialidade } from '../../../../api/dentistas';


class DentistasEspecialidade extends Component {

		state = {
			formulario: [
				{ type: 'text', name: 'titulo', label: 'Especialidade' }
			],
			alerta: {
				tipo: '',
				msg: '',
				abrir: false
			},
			especialidade: '',
			cadastro: true,
			await: true
		}

		async componentWillMount() {
			const { clinica } = this.props;

			if (this.props.match.params.especialidade_id) {
				var objEspecialidade = await buscarEspecialidade(clinica._id, this.props.match.params.especialidade_id);
				this.setState({ cadastro: false, especialidade: objEspecialidade, await: false })
			} else {
				this.setState({ await: false });
			}
		}

		editaEspecialidade(objeto) {
			editarEspecialidade(objeto)
				.then(r => {
					if (r) {
						this.setState({
							alerta: {
								tipo: 'success',
								msg: 'Especialidade editada com sucesso',
								abrir: true
							}
						});
						setTimeout(() => this.props.history.goBack(), 3000)
					} else {
						this.setState({
							alerta: {
								tipo: 'error',
								msg: 'Infelizmente não foi possível salvar a especialidade!',
								abrir: true
							}
						})
					}
				})
				.then(this.setState({ alerta: { abrir: false } }))
		}

		salvarEspecialidade(objeto) {
			const { clinica } = this.props;

			cadastrarEspecialidade(clinica, objeto.titulo)
				.then(r => {
					if (r) {
						this.setState({
							alerta: {
								tipo: 'success',
								msg: 'Especialidade cadastrada com sucesso',
								abrir: true
							}
						});
						setTimeout(() => this.props.history.goBack(), 3000)
					} else {
						this.setState({
							alerta: {
								tipo: 'error',
								msg: 'Infelizmente não foi possível salvar a especialidade!',
								abrir: true
							}
						})
					}
				})
				.then(this.setState({ alerta: { abrir: false } }))
		}

		render() {
			return (
				<div>
					{this.state.alerta.abrir &&
						<Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
					}
					{!this.state.await &&
						<FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.especialidade} action={this.state.cadastro ? this.salvarEspecialidade.bind(this) : this.editaEspecialidade.bind(this)} />
					}
				</div>
			);
		}
	}

function mapStateToProps(state) {

	const { auth } = state;

	return {
		user: auth.user,
		clinica: auth.clinic
	};
}
export default connect(mapStateToProps)(DentistasEspecialidade);