import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import Paper from '@material-ui/core/Paper';
import { buscarProtetico, cadastrarProtetico, editarProtetico } from '../../../../api/proteticos';


class ProteticosCadastro extends Component {

    state = {
        formulario: [
            { type: 'text', name: 'codigo', label: 'Código' },
            { type: 'text', name: 'nome', label: 'Nome' },
            { type: 'text', name: 'adress', label: 'Endereço Comercial' },
            { type: 'text', name: 'city', label: 'Cidade' },
            { type: 'text', name: 'uf', label: 'UF' },
            { type: 'number', name: 'cep', label: 'CEP' },
            { type: 'text', name: 'cpf', label: 'CPF' },
            { type: 'text', name: 'rg', label: 'RG' },
            { type: 'number', name: 'inscEstadual', label: 'Inscrição Estadual' },
            { type: 'number', name: 'cnpj', label: 'CNPJ' },
            { type: 'number', name: 'telefone', label: 'Fone' },
            { type: 'text', name: 'email', label: 'E-mail' },
            { type: 'text', name: 'site', label: 'Site' }
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        protetico: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.protetico_id) {
            var objProtetico = await buscarProtetico(clinica._id, this.props.match.params.protetico_id);
            this.setState({ cadastro: false, protetico: objProtetico, await: false })
        } else {
            this.setState({ await: false });
        }
    }

    editaProtetico(objeto) {
        editarProtetico(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Protético editado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o Protético!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarProtetico(objeto) {
        const { clinica } = this.props;
        cadastrarProtetico(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Protético cadastrado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o Protético!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                <Paper>
                    {this.state.alerta.abrir &&
                        <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                    }
                    {!this.state.await &&
                        <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.protetico} action={this.state.cadastro ? this.salvarProtetico.bind(this) : this.editaProtetico.bind(this)} />
                    }
                </Paper>
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ProteticosCadastro);