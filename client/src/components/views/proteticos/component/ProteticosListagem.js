import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { buscarProteticos, deletarProtetico } from "../../../../api/proteticos";

import TableComponent from '../../../table/TableComponent';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import PageviewTwoTone from '@material-ui/icons/PageviewTwoTone';


class ProteticosListagem extends Component {
    state = {
        proteticos: []
    }

    deletaProtetico(objeto) {
        const { clinica } = this.props;
        deletarProtetico(objeto, clinica._id);
    }

    componentWillMount() {
        buscarProteticos(this.props.clinica._id)
            .then(proteticos => {
                var listaProteticos = []
                proteticos.length > 0 && proteticos.map((protetico, index) => {
                    var novoProtetico = {};
                    novoProtetico.codigo = protetico.codigo;
                    novoProtetico.proteticoId = protetico._id;
                    novoProtetico.nome = protetico.nome;
                    novoProtetico.email = protetico.email;
                    novoProtetico.telefone = protetico.telefone;
                    novoProtetico.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/proteticos/cadastrarProtetico/${protetico._id}`)} />,
                        <Tooltip title={"Listar Tabela de Serviços"}   >
                            <IconButton onClick={() => this.props.history.push(`/proteticos/tabelaPrecoVinculaProtetico/${protetico._id}`)}>
                                <PageviewTwoTone />
                            </IconButton>
                        </Tooltip>
                    ]
                    listaProteticos.push(novoProtetico);
                });
                this.setState({ proteticos: listaProteticos })
            })
    }
    render() {
        const titulo = "Protéticos",
            colunas = [
                { label: 'Codigo', name: 'codigo' },
                { label: 'Nome', name: 'nome' },
                { label: 'Email', name: 'email' },
                { label: 'Telefone', name: 'telefone' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.proteticos,
            inserir = {
                nome: "Cadastrar Protético",
                link: "cadastrarProtetico"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaProtetico.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ProteticosListagem);