import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect, Link } from 'react-router-dom';

import ProteticosListagem from './component/ProteticosListagem';
import ProteticosCadastro from './component/ProteticosCadastro';

// Importacoes para a navbar
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import TableChart from '@material-ui/icons/TableChart';
import TabelaPrecoListagem from './component/TabelaPrecoListagem';
import TabelaPreco from './component/TabelaPreco';
import TabelaPrecoProteticosRelacao from './component/TabelaPrecoProteticosRelacao';
import TabelaPrecoVinculaProteticos from './component/TabelaPrecoVinculaProteticos';

class ProteticosRouter extends Component {
    state = {
        value: 0
    }

    trocarTab(tab) {
        this.setState({ value: tab })
    }

    render() {
        const { match } = this.props;
        var { value } = this.state;

        const tab = {
            lista: [
                { url: "listagem", numero: 0 },
                { url: "listagemServicos", numero: 1 }
            ],
            tabAtiva: value
        }
        return (
            <div>
                <BottomNavigation
                    className="navbar"
                    value={value}
                    onChange={(event, newValue) => {
                        this.setState({ value: newValue });
                    }}
                    showLabels
                >
                    <BottomNavigationAction onClick={() => this.props.history.push(`${match.url}/listagem`)} label="Protéticos" icon={<PeopleOutlineIcon />} />
                    <BottomNavigationAction onClick={() => this.props.history.push(`${match.url}/config`)} label="Serviços Protéticos" icon={<TableChart />} />
                </BottomNavigation>
                <Switch>
                    <Route path={`${match.url}/config`} render={props => <TabelaPrecoListagem {...props} tab={tab} trocarTab={this.trocarTab.bind(this)} />} />
                    <Route exact path={`${match.url}/listagem`} render={props => <ProteticosListagem {...props} tab={tab} trocarTab={this.trocarTab.bind(this)} />} />
                    <Route exact path={`${match.url}/cadastrarProtetico/:protetico_id?`} render={props => <ProteticosCadastro {...props} />} />
                    <Route exact path={`${match.url}/cadastrarTabelaPreco/:tabelaPreco_id?`} render={props => <TabelaPreco {...props} />} />
                    <Route exact path={`${match.url}/tabelaPrecoVinculaProtetico/:protetico_id`} render={props => <TabelaPrecoProteticosRelacao {...props} />} />
                    <Route exact path={`${match.url}/vincularTabelaPreco/:protetico_id?`} render={props => <TabelaPrecoVinculaProteticos {...props} />} />
                    <Route exact path={`${match.url}/`} render={() => <Redirect to={`${match.url}/listagem`} />} />
                </Switch>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectProtetico: state.patientsCreation.selectProtetico,
    };
}
export default connect(mapStateToProps)(ProteticosRouter);