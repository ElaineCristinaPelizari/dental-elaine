import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import Paper from '@material-ui/core/Paper';
import { buscarConvenio, editarConvenio, cadastrarConvenio } from '../../../../api/convenios';

class ConveniosCadastro extends Component {

    state = {
        formulario: [
            { type: 'text', name: 'codigo', label: 'Código' },
            { type: 'text', name: 'nomeConvenio', label: 'Nome Convenio' },
            { type: 'text', name: 'endereco', label: 'Endereço' },
            { type: 'text', name: 'cidade', label: 'Cidade' },
            { type: 'text', name: 'uf', label: 'UF' },
            { type: 'number', name: 'cep', label: 'CEP' },
            { type: 'text', name: 'telefone', label: 'Telefone' },
            { type: 'number', name: 'cnpj', label: 'CNPJ' },
            { type: 'number', name: 'inscEstadual', label: 'Insc. Estadual' },
            { type: 'text', name: 'email', label: 'E-Mail' },
            { type: 'text', name: 'site', label: 'Site' },
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        convenio: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.convenio_id) {
            var objConvenio = await buscarConvenio(clinica._id, this.props.match.params.convenio_id);
            this.setState({ cadastro: false, convenio: objConvenio, await: false })
        } else {
            this.setState({ await: false });
        }
    }

    editaConvenio(objeto) {
        editarConvenio(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Convenio editada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o convenio!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarConvenio(objeto) {
        const { clinica } = this.props;
        cadastrarConvenio(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Convenio cadastrado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o convenio!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                <Paper>
                    {this.state.alerta.abrir &&
                        <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                    }
                    {!this.state.await &&
                        <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.convenio} action={this.state.cadastro ? this.salvarConvenio.bind(this) : this.editaConvenio.bind(this)} />
                    }
                </Paper>
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ConveniosCadastro);