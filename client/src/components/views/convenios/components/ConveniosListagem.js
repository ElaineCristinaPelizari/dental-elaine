import React, { Component } from 'react';
import { connect } from 'react-redux';

import TableComponent from '../../../table/TableComponent';
//Icone de arquivar
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import { deletarConvenio, buscarConvenios } from '../../../../api/convenios';

class ConveniosListagem extends Component {
    state = {
        convenios: []
    }
    deletaConvenio(objeto) {
        const { clinica } = this.props;
        deletarConvenio(objeto, clinica._id);
    }

    componentWillMount() {
        buscarConvenios(this.props.clinica._id)
            .then(convenios => {
                var listaConvenios = []
                convenios.length > 0 && convenios.map((convenio, index) => {
                    var novo = {};
                    novo.codigo = convenio.codigo;
                    novo.convenioId = convenio._id;
                    novo.nomeConvenio = convenio.nomeConvenio;
                    novo.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/convenios/cadastrarConvenio/${convenio._id}`)} />,
                    ]
                    listaConvenios.push(novo);
                });
                this.setState({ convenios: listaConvenios })
            })
    }
    render() {
        const titulo = "Convênios",
            colunas = [
                { label: 'Codigo', name: 'codigo' },
                { label: 'Nome Convenio', name: 'nomeConvenio' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.convenios,
            inserir = {
                nome: "Cadastrar Convenio",
                link: "cadastrarConvenio"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaConvenio.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ConveniosListagem);