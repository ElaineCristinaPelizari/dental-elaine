import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableComponent from '../../../table/TableComponent';
import { deletarAnamnese, buscarAnamneses } from '../../../../api/anamnese';
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import PageviewTwoTone from '@material-ui/icons/PageviewTwoTone';
import parseISO from 'date-fns';

class AnamneseListagem extends Component {
    state = {
        anamneses: []
    }

    deletaAnamnese(objeto) {
        const { clinica } = this.props;
        deletarAnamnese(objeto, clinica._id);
    }

    componentWillMount() {
        buscarAnamneses(this.props.clinica._id)
            .then(anamneses => {
                var listaAnamneses = []
                console.log(anamneses);
                anamneses.length > 0 && anamneses.map((anamnese, index) => {
                    var novo = {};
                    novo.nomeFicha = anamnese.nomeFicha;
                    novo.data = (anamnese.data);
                    novo.anamneseId = anamnese._id;
                    novo.funcoes = [
                        //<EditTwoTone key={index} onClick={() => this.props.history.push(`/anamnese/cadastrarAnamnese/${anamnese._id}`)} />,
                        <Tooltip title={"Visualizar Ficha"} >
                            <IconButton onClick={() => this.props.history.push(`/anamnese/listarFicha/${anamnese._id}`)}>
                                <PageviewTwoTone />
                            </IconButton>
                        </Tooltip>,
                    ]

                    listaAnamneses.push(novo);
                });
                this.setState({ anamneses: listaAnamneses })
            })
    }

    render() {
        const titulo = "Ficha Anamnese",
            colunas = [
                { label: 'Id', name: 'anamneseId' },
                { label: 'Nome Ficha', name: 'nomeFicha' },
                { label: 'Data de Criação', name: 'data' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.anamneses,
            inserir = {
                nome: "Cadastrar Ficha Anamnese",
                link: "cadastrarAnamnese"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaAnamnese.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(AnamneseListagem);