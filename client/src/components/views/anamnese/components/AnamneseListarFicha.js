import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableComponent from '../../../table/TableComponent';
import { buscarFichaPergunta } from '../../../../api/anamnese';

class AnamneseListarFicha extends Component {
    state = {

        anamneses: [],
        idAnamnese: ''

    }

    componentWillMount() {
        console.log(this.props.match.params.anamnese_id)
        buscarFichaPergunta(this.props.clinica._id, this.props.match.params.anamnese_id)
            .then(result => {
                console.log(result);
                var anamnese = []
                result.perguntas.length > 0 && result.perguntas.map((perguntas, index) => {
                    var novo = {};
                    novo.pergunta = perguntas.pergunta;
                    novo.tip = perguntas.tip;
                    novo.tamanho = perguntas.tamanho;
                    novo.opcao = perguntas.opcao;
                    novo._id = perguntas._id;
                    anamnese.push(novo);
                });
                this.setState({ anamneses: anamnese, idAnamnese: this.props.match.params.anamnese_id })
            })
            .catch(erro => console.log(erro))
    }

    render() {
        const titulo = "Detalhes Ficha",
            colunas = [
                { label: 'Pergunta', name: 'pergunta' },
                { label: 'Tipo', name: 'tip' },
                { label: 'Tamanho', name: 'tamanho' },
                { label: 'Opção de Resposta', name: 'opcao' },
            ],
            objetos = this.state.anamneses

        return (
            <TableComponent titulo={titulo} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(AnamneseListarFicha);