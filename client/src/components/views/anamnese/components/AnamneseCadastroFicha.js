import React, { Component } from 'react';
import { connect } from 'react-redux';
import Notificacao from '../../../notificacao/Notificacao';
import { cadastrarAnamnese } from '../../../../api/anamnese';
import { Input, Button, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import DateTimeInput from '../../../forms/date/DateTimeInput';


class AnanmneseCadastroFicha extends Component {

    state = {
        anamnese: {
            nomeFicha: String,
            data: Date,
            perguntas: [{
                pergunta: String,
                tip: String,
                tamanho: Number,
                opcao: String
            }]
        },

        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
    }

    addPergunta() {
        this.setState({
            anamnese: { perguntas: [...this.state.anamnese.perguntas, {}], nomeFicha: this.state.anamnese.nomeFicha, data: this.state.anamnese.data }
        })
        console.log(this.state.anamnese);
    }

    handleChange(e, index) {
        // console.log(e.target.value);
        if (e.target.id == 'nomeFicha') {
            this.state.anamnese.nomeFicha = e.target.value;
        }
        if (e.target.id == 'data') {
            this.state.anamnese.data = e.target.value;
        }
        if (e.target.id == 'pergunta') {
            this.state.anamnese.perguntas[index].pergunta = e.target.value;
        }
        if (e.target.id == 'tipo') {
            this.state.anamnese.perguntas[index].tip = e.target.value;
        }
        if (e.target.id == 'tamanho') {
            this.state.anamnese.perguntas[index].tamanho = e.target.value;
        }
        if (e.target.id == 'opcao') {
            this.state.anamnese.perguntas[index].opcao = e.target.value;
        }
        // console.log("nomeFicha: ", this.state.anamnese.nomeFicha);
        this.setState({ anamnese: this.state.anamnese })
    }

    handleRemove(index) {
        this.state.anamnese.perguntas.splice(index, 1)
        this.setState({ anamnese: this.state.anamnese })
    }

    handleSubmit(objeto) {
        const { clinica } = this.props;
        objeto = this.state.anamnese;
        console.log("submit", objeto);
        cadastrarAnamnese(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Perguntas salvas com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar as perguntas!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                <h3>Configuração Anamnese</h3>

                {this.state.alerta.abrir &&
                    <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                }

                <DateTimeInput
                    placeholder="Data de Criação" id="data"
                    onChange={(e) => this.handleChange(e)} value={this.state.anamnese.data}
                />

                <hr />

                <Input
                    placeholder="Nome da Ficha" id="nomeFicha"
                    onChange={(e) => this.handleChange(e)} value={this.state.anamnese.nomeFicha}
                />

                {
                    this.state.anamnese.perguntas.map((perguntas, index) => {
                        return (

                            <form>
                                <div key={index} >
                                    <hr />
                                    <Input
                                        placeholder="Pergunta" id="pergunta"
                                        onChange={(e) => this.handleChange(e, index)} value={perguntas.pergunta}
                                    />
                                    <hr />
                                    <Input
                                        placeholder="Tipo" id="tipo"
                                        onChange={(e) => this.handleChange(e, index)} value={perguntas.tip}
                                    />
                                    <hr />
                                    <Input
                                        placeholder="Tamanho" id="tamanho"
                                        onChange={(e) => this.handleChange(e, index)} value={perguntas.tamanho}
                                    />
                                    <hr />
                                    <Input
                                        placeholder="Opção de Resposta" id="opcao"
                                        onChange={(e) => this.handleChange(e, index)} value={perguntas.opcao}
                                    />

                                    <IconButton aria-label="delete" size="medium" onClick={() => this.handleRemove(index)}>
                                        <DeleteIcon fontSize="inherit" />
                                    </IconButton>

                                </div>
                            </form>
                        )
                    })
                }

                <hr />

                <Button variant="contained" color="primary" onClick={(e) => this.addPergunta(e)}>Adicionar Nova Pergunta</Button>

                <hr />

                <Button variant="contained" color="secondary" onClick={(e) => this.handleSubmit(e)}>Salvar</Button>
            </div >
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(AnanmneseCadastroFicha);
