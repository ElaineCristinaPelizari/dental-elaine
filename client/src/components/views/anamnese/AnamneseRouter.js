import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect, Link } from 'react-router-dom';
// Importacoes para a navbar
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import SettingsTwoTone from '@material-ui/icons/SettingsTwoTone';
import AnamneseListagem from './components/AnamneseListagem';
import AnamneseCadastroFicha from './components/AnamneseCadastroFicha';
import AnamneseListarFicha from './components/AnamneseListarFicha';

class AnamneseRouter extends Component {

    state = {
        value: 0
    }

    trocarTab(tab) {
        this.setState({ value: tab })
    }

    render() {

        const { match } = this.props;
        var { value } = this.state;

        const tab = {
            lista: [
                { url: "listagem", numero: 0 },
            ],
            tabAtiva: value
        }
        return (
            <div>
                <BottomNavigation
                    className="navbar"
                    value={value}
                    onChange={(event, newValue) => {
                        this.setState({ value: newValue });
                    }}
                    showLabels
                >
                    <BottomNavigationAction onClick={() => this.props.history.push(`${match.url}/listagem`)} label="Configuração Anamnese" icon={<SettingsTwoTone />} />
                </BottomNavigation>
                <Switch>
                    <Route exact path={`${match.url}/listagem`} render={props => <AnamneseListagem {...props} tab={tab} trocarTab={this.trocarTab.bind(this)} />} />
                    <Route exact path={`${match.url}/cadastrarAnamnese/:anamnese_id?`} render={props => <AnamneseCadastroFicha {...props} />} />
                    <Route exact path={`${match.url}/listarFicha/:anamnese_id`} render={props => <AnamneseListarFicha {...props} />} />
                    <Route exact path={`${match.url}/`} render={() => <Redirect to={`${match.url}/listagem`} />} />
                </Switch>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        //selectedPatient: state.patientsCreation.selectedPatient,
    };
}
export default connect(mapStateToProps)(AnamneseRouter);