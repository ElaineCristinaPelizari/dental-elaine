import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import { buscarAnomalia, editarAnomalia, cadastrarAnomalia } from '../../../../api/anomalias';
import InputBase from '@material-ui/core/InputBase';

class AnomaliaCadastro extends Component {

    state = {
        formulario: [
            { type: 'text', name: 'codigo', label: 'Código', required: true },
            { type: 'text', name: 'descricao', label: 'Descrição',  required: true }
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        anomalia: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.anomalia_id) {
            var objAnomalia = await buscarAnomalia(clinica._id, this.props.match.params.anomalia_id);
            this.setState({ cadastro: false, anomalia: objAnomalia, await: false })
        } else {
            this.setState({ await: false });
        }
    }

    editaAnomalia(objeto) {
        editarAnomalia(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Anomalia editada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar a anomalia!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarAnomalia(objeto) {
        const { clinica } = this.props;
        cadastrarAnomalia(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Anomalia cadastrada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar a anomalia!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                {this.state.alerta.abrir &&
                    <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                }
                {!this.state.await &&
                    <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.anomalia} action={this.state.cadastro ? this.salvarAnomalia.bind(this) : this.editaAnomalia.bind(this)} />
                }
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps, { cadastrarAnomalia, editarAnomalia })(AnomaliaCadastro);