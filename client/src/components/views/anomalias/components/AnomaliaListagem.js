import React, { Component } from 'react';
import { connect } from 'react-redux';

import TableComponent from '../../../table/TableComponent';
//Icone de arquivar
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import { deletarAnomalia, buscarAnomalias } from '../../../../api/anomalias';


class AnomaliaListagem extends Component {
    state = {
        anomalias: []
    }
    deletaAnomalia(objeto) {
        const { clinica } = this.props;
        deletarAnomalia(objeto, clinica._id);
    }

    componentWillMount() {
        buscarAnomalias(this.props.clinica._id)
            .then(anomalias => {
                var listaAnomalias = []
                anomalias.length > 0 && anomalias.map((anomalia, index) => {
                    var NovaAnomalia = {};
                    NovaAnomalia.codigo = anomalia.codigo;
                    NovaAnomalia.anomaliaId = anomalia._id;
                    NovaAnomalia.descricao = anomalia.descricao;
                    NovaAnomalia.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/anomalias/cadastrarAnomalia/${anomalia._id}`)} />,
                    ]
                    listaAnomalias.push(NovaAnomalia);
                });
                this.setState({ anomalias: listaAnomalias })
            })
    }
    render() {
        const titulo = "Anomalias",
            colunas = [
                { label: 'Codigo', name: 'codigo' },
                { label: 'Descrição', name: 'descricao' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.anomalias,
            inserir = {
                nome: "Cadastrar Anomalia",
                link: "cadastrarAnomalia"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaAnomalia.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(AnomaliaListagem);