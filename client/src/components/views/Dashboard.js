import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { css } from 'aphrodite/no-important';
import { gridStyles } from './DashboardStyles';

import SideMenu from '../navigation/SideMenu';
import TopBar from '../bars/TopBar';
import SystemMsg from '../notifications/SystemMsg';

import resumeDashboard from './resume/resumeDashboard';
import PatientsRouter from './patients/PatientsRouter';
import StorageRouter from './storage/StorageRouter';
import AuditRouter from './audit/AuditRouter';
import AgendaRouter from './agenda/AgendaRouter';
import ConfigurationRouter from './configurations/ConfigurationRouter';
import WindowsController from './windows/WindowsController';
import financeAccount from './finances/financeAccount';

import ClientsRouter from './clients/ClientsRouter';
import PacientesRouter from './pacientes/PacientesRouter';
import ServicosRouter from './servicos/ServicosRouter';
import DentistasRoutes from './dentistas/DentistasRouter';
import ProteticosRouter from './proteticos/ProteticosRouter';
import ContasPagarRouter from './contasPagar/ContasPagarRouter';
import ConveniosRouter from './convenios/ConveniosRouter';
import FornecedorRouter from './fornecedor/FornecedorRouter';
import AnamneseRouter from './anamnese/AnamneseRouter';

// 'Dashboard' will manage the routes inside the app
function Dashboard(props) {
	const { history, match } = props;

	if(props.auth.authenticated){
	return (
		<div className={css(gridStyles.grid)}>
			<SideMenu match={match} history={history} />

			<TopBar history={history} />

			<div className={css(gridStyles.content)}>
				<SystemMsg />

				<Route path={`${match.url}pacientes`} component={PacientesRouter} />
				<Route path={`${match.url}servicos`} component={ServicosRouter} />
				<Route path={`${match.url}patients`} component={PatientsRouter} />
				<Route path={`${match.url}clients`} component={ClientsRouter} />
				<Route path={`${match.url}dentistas`} component={DentistasRoutes} />
				<Route path={`${match.url}proteticos`} component={ProteticosRouter} />
				{/* <Route path={`${match.url}anomalias`} component={AnomaliasRouter} /> */}
				<Route path={`${match.url}contas`} component={ContasPagarRouter} />
				<Route path={`${match.url}convenios`} component={ConveniosRouter} />
				<Route path={`${match.url}fornecedor`} component={FornecedorRouter} />
				<Route path={`${match.url}agenda`} component={AgendaRouter} />
				<Route path={`${match.url}finances/:type?/:account?/:since?/:to?`} component={financeAccount} />
				<Route exact path={`${match.url}finances`} render={() => <Redirect to={`${match.url}finances/general/all/${new Date().toISOString().slice(0,10)}/infinite`} />} />
				<Route path={`${match.url}storage`} component={StorageRouter} />
				<Route path={`${match.url}communication`} component={() => <h1>communication</h1>} />
				<Route path={`${match.url}bi`} component={() => <h1>bi</h1>} />
				<Route path={`${match.url}audit`} component={AuditRouter} />
				<Route path={`${match.url}anamnese`} component={AnamneseRouter} />

				<Route path={`${match.url}configuration`} component={ConfigurationRouter} />
				
				<Route exact path={match.url} component={resumeDashboard} />

				<WindowsController/>
			</div>
		</div>
	);
    }else{
		return <div><h1> Carregando </h1></div>
	}
	
}

export default Dashboard;
