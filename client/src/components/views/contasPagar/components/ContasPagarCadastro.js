import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import Paper from '@material-ui/core/Paper';
import { buscarConta, cadastrarConta, editarConta, buscarDescricoes } from '../../../../api/contasPagar';

class ContasPagarCadastro extends Component {

    state = {
        formulario: [
            { type: 'text', name: 'nome', label: 'Nome do Credor' },
            { type: 'text', name: 'nota', label: 'Nota' },
            { type: 'text', name: 'duplicada', label: 'Duplicata' },
            { type: 'text', name: 'dataVenc', label: 'Data de Vencimento' },
            { type: 'number', name: 'indice', label: 'Indice' },
            { type: 'number', name: 'valor', label: 'Valor Total' },
            { type: 'number', name: 'nPrestacao', label: 'Nº Parcelas' },
            { type: 'number', name: 'totalPrestacao', label: 'Total Parcelas' },
            { type: 'text', name: 'observacao', label: 'Observação' }
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        conta: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        if (this.props.match.params.conta_id) {
            var objConta = await buscarConta(clinica._id, this.props.match.params.conta_id);
            console.log(objConta);
            this.setState({ cadastro: false, conta: objConta, await: false })
        } else {
            this.setState({ await: false });
        }

        var objDescricao = await buscarDescricoes(clinica._id);
        var formulario = this.state.formulario;

        var selectDescricao = {
            type: 'select',
            name: 'descricao',
            label: 'Detalhe da Conta',
            values: []
        };

        objDescricao.map(obj => {
            var novo = {};
            novo.name = obj.descricao;
            novo.value = obj._id;
            selectDescricao.values.push(novo)
        })

        formulario.push(selectDescricao);
        this.setState({ await: false, conta_id: this.props.match.params.conta_id, formulario: formulario })
    }

    editaConta(objeto) {
        editarConta(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Conta editada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o lançamento da Conta!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarConta(objeto) {
        const { clinica } = this.props;
        cadastrarConta(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Conta cadastrada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o lançamento da conta!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                <Paper>
                    {this.state.alerta.abrir &&
                        <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                    }
                    {!this.state.await &&
                        <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.conta} action={this.state.cadastro ? this.salvarConta.bind(this) : this.editaConta.bind(this)} />
                    }
                </Paper>
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ContasPagarCadastro);