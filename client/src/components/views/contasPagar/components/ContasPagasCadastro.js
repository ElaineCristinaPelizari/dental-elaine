import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import Paper from '@material-ui/core/Paper';
import { buscarContasPagas, editarContasPagas, cadastrarContasPagas, buscarContas } from '../../../../api/contasPagar';

class ContasPagasCadastro extends Component {

    state = {
        formulario: [
            { type: 'number', name: 'valorPago', label: 'Valor Pago' },
            { type: 'text', name: 'dataPagamento', label: 'Data do Pagamento' },
            { type: 'text', name: 'observacao', label: 'Observação' },
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        contaPaga: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;

        var formulario = this.state.formulario;

        if (this.props.match.params.contaPaga_id) {
            var mostrarTituloConta = { type: 'text', name: 'nome', label: 'Descrição da conta' }

            formulario = [mostrarTituloConta].concat(formulario);

            var objContaPaga = await buscarContasPagas(clinica._id, this.props.match.params.contaPaga_id);
            objContaPaga.nome = objContaPaga.contasPagar.nome;

            this.setState({ cadastro: false, contaPaga: objContaPaga, await: false, formulario: formulario })
        } else {

            var objConta = await buscarContas(clinica._id);

            var selectConta = {
                type: 'select',
                name: 'contasPagar',
                label: 'Selecionar Conta',
                values: []
            };

            objConta.map(obj => {
                var pago = 0;

                obj.contasPagas.length > 0 && obj.contasPagas.map(de => {
                    pago = pago + de.valorPago;

                });
                if ((obj.valor - pago) > 0) {
                    var novo = {};
                    novo.name = obj.nome;
                    novo.value = obj._id;
                    novo.valor = obj.valor;
                    selectConta.values.push(novo)
                }
            })
            formulario = [selectConta].concat(formulario);
            this.setState({ await: false, contaPaga_id: this.props.match.params.contaPaga_id, formulario: formulario });
        }
    }

    editaContaPaga(objeto) {
        editarContasPagas(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Conta Paga editada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o lançamento da Conta Paga!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarContaPaga(objeto) {
        const { clinica } = this.props;
        cadastrarContasPagas(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Conta Paga cadastrada com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o lançamento da conta paga!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                <Paper>
                    {this.state.alerta.abrir &&
                        <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                    }
                    {!this.state.await &&
                        <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.contaPaga} action={this.state.cadastro ? this.salvarContaPaga.bind(this) : this.editaContaPaga.bind(this)} />
                    }
                </Paper>
            </div >
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ContasPagasCadastro);