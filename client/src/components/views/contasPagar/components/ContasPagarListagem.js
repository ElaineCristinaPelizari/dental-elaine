import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableComponent from '../../../table/TableComponent';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import { deletarConta, buscarContas } from '../../../../api/contasPagar';

class ContasPagarListagem extends Component {
    state = {
        contas: []
    }

    deletaConta(objeto) {
        const { clinica } = this.props;
        deletarConta(objeto, clinica._id);
    }

    componentWillMount() {
        buscarContas(this.props.clinica._id)
            .then(contas => {
                var listaContas = []

                contas.length > 0 && contas.map((conta, index) => {
                    var novo = {};
                    var pago = 0;

                    conta.contasPagas.map(detalheConta => {
                        pago = pago + detalheConta.valorPago;
                    })

                    var valorDevido = (conta.valor - pago);

                    if (valorDevido === 0) {
                        return false;
                    } else {
                        novo.nome = conta.nome;
                        novo.contaId = conta._id;
                        novo.dataVenc = conta.dataVenc;
                        novo.valorDevido = parseFloat(valorDevido).toFixed(2);
                        novo.valor = parseFloat(conta.valor).toFixed(2);
                        novo.nPrestacao = conta.nPrestacao;
                        novo.descricao = conta.descricao.descricao;
                        novo.funcoes = [
                            <EditTwoTone key={index} onClick={() => this.props.history.push(`/contas/cadastrarConta/${conta._id}`)} />,
                        ]
                        listaContas.push(novo);
                    }
                });
                this.setState({ contas: listaContas })
            })
    }

    render() {
        const titulo = "Contas A Pagar",
            colunas = [
                { label: 'Nome do Credor', name: 'nome' },
                { label: 'Data Vencimento', name: 'dataVenc' },
                { label: 'Valor Devido', name: 'valorDevido' },
                { label: 'Valor Total', name: 'valor' },
                { label: 'Nº Prestação', name: 'nPrestacao' },
                { label: 'Detalhe da Conta', name: 'descricao' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.contas,
            inserir = {
                nome: "Cadastrar Conta",
                link: "cadastrarConta"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaConta.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ContasPagarListagem);