import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableComponent from '../../../table/TableComponent';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import { deletarContasPagas, ListarContasPagas } from '../../../../api/contasPagar';

class ContasPagasListagem extends Component {
    state = {
        contasPagas: []
    }

    deletaContaPaga(objeto) {
        const { clinica } = this.props;
        deletarContasPagas(objeto, clinica._id);
    }

    componentWillMount() {
        ListarContasPagas(this.props.clinica._id)
            .then(contasPagas => {
                var listaContasPagas = []
                contasPagas.length > 0 && contasPagas.map((contaPaga, index) => {
                    var novo = {};
                    novo.contasPagar = contaPaga.contasPagar.nome;
                    novo.valorPago = parseFloat(contaPaga.valorPago).toFixed(2);
                    novo.contaPagaId = contaPaga._id;
                    novo.dataPagamento = contaPaga.dataPagamento;
                    novo.observacao = contaPaga.observacao;
                    novo.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/contas/cadastrarContasPagas/${contaPaga._id}`)} />,
                    ]
                    listaContasPagas.push(novo);
                });
                this.setState({ contasPagas: listaContasPagas })
            })
    }
    render() {
        const titulo = "Contas Pagas",
            colunas = [
                { label: 'Conta Paga', name: 'contasPagar' },
                { label: 'Valor Pago', name: 'valorPago' },
                { label: 'Data Pagamento', name: 'dataPagamento' },
                { label: 'Observação', name: 'observacao' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.contasPagas,
            inserir = {
                nome: "Cadastrar Contas Pagas",
                link: "cadastrarContasPagas"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaContaPaga.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(ContasPagasListagem);