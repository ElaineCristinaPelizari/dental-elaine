import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import Paper from '@material-ui/core/Paper';
import { buscarTipoPagamento, editarTipoPagamento, cadastrarTipoPagamento } from '../../../../api/formaPagamento';

class FormaPagamentoCadastro extends Component {

    state = {
        formulario: [
            { type: 'text', name: 'codigo', label: 'Código' },
            { type: 'text', name: 'tipo', label: 'Tipo Pagamento' },
        ],
        alerta: {
            tipo: '',
            msg: '',
            abrir: false
        },
        formaPagamento: '',
        cadastro: true,
        await: true
    }

    async componentWillMount() {
        const { clinica } = this.props;
        console.log(this.props.match.params.formaPagamento_id);
        if (this.props.match.params.formaPagamento_id) {
            console.log("oi");
            var objFormaPagamento = await buscarTipoPagamento(clinica._id, this.props.match.params.formaPagamento_id);
            this.setState({ cadastro: false, formaPagamento: objFormaPagamento, await: false })
        } else {
            this.setState({ await: false });
        }
    }

    editaFormaPagamento(objeto) {
        editarTipoPagamento(objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Tipo de pagamento editado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o tipo de pagamento!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    salvarFormaPagamento(objeto) {
        const { clinica } = this.props;
        cadastrarTipoPagamento(clinica, objeto)
            .then(r => {
                if (r) {
                    this.setState({
                        alerta: {
                            tipo: 'success',
                            msg: 'Tipo de pagamento cadastrado com sucesso',
                            abrir: true
                        }
                    });
                    setTimeout(() => this.props.history.goBack(), 3000)
                } else {
                    this.setState({
                        alerta: {
                            tipo: 'error',
                            msg: 'Infelizmente não foi possível salvar o Tipo de pagamento!',
                            abrir: true
                        }
                    })
                }
            })
            .then(this.setState({ alerta: { abrir: false } }))
    }

    render() {
        return (
            <div>
                <Paper>
                    {this.state.alerta.abrir &&
                        <Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg} />
                    }
                    {!this.state.await &&
                        <FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.formaPagamento} action={this.state.cadastro ? this.salvarFormaPagamento.bind(this) : this.editaFormaPagamento.bind(this)} />
                    }
                </Paper>
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { auth } = state;

    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(FormaPagamentoCadastro);