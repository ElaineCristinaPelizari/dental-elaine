import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableComponent from '../../../table/TableComponent';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import { deletarTipoPagamento, buscasTipoPagamentos } from '../../../../api/formaPagamento';


class FormaPagamentoListagem extends Component {
    state = {
        formaPagamentos: []
    }

    deletaTipoPagamento(objeto) {
        const { clinica } = this.props;
        deletarTipoPagamento(objeto, clinica._id);
    }

    componentWillMount() {
        buscasTipoPagamentos(this.props.clinica._id)
            .then(formaPagamentos => {
                var listaFormaPagamentos = []
                formaPagamentos.length > 0 && formaPagamentos.map((formaPagamento, index) => {
                    var novo = {};
                    novo.codigo = formaPagamento.codigo;
                    novo.tipo = formaPagamento.tipo;
                    novo.formaPagamentoId = formaPagamento._id;
                    novo.funcoes = [
                        <EditTwoTone key={index} onClick={() => this.props.history.push(`/configuration/formaPagamento/cadastrarFormaPagamento/${formaPagamento._id}`)} />,
                    ]
                    listaFormaPagamentos.push(novo);
                });
                this.setState({ formaPagamentos: listaFormaPagamentos })
            })
    }
    render() {
        const titulo = "Tipos de Pagamento",
            colunas = [
                { label: 'Código', name: 'codigo' },
                { label: 'Tipo', name: 'tipo' },
                { label: '', name: 'funcoes', options: { filter: false, sort: false } },
            ],
            objetos = this.state.formaPagamentos,
            inserir = {
                nome: "Cadastrar Forma Pagamento",
                link: "cadastrarFormaPagamento"
            }
        return (
            <TableComponent funcoes={{ deletar: this.deletaTipoPagamento.bind(this) }} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props} />
        );
    }
}

function mapStateToProps(state) {
    const { auth } = state;
    return {
        user: auth.user,
        clinica: auth.clinic
    };
}
export default connect(mapStateToProps)(FormaPagamentoListagem);