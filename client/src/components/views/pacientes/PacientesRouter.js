import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect, Link } from 'react-router-dom';

import PacientesListagem from './components/PacientesListagem';
import PacientesArquivados from './components/PacientesArquivados';
import PacientesConfig from './components/PacientesConfig';

// Importacoes para a navbar
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FaceTwoTone from '@material-ui/icons/FaceTwoTone';
import ArchiveTwoTone from '@material-ui/icons/ArchiveTwoTone';
import SettingsTwoTone from '@material-ui/icons/SettingsTwoTone';

class PacientesRouter extends Component {

	state = {
		value:0
	}
	
	trocarTab(tab){
		this.setState({value:tab})
	}

	render(){
		
		const { match } = this.props;
		var { value } = this.state;

		const tab = {
			lista:[
				{url:"listagem", numero:0 },
				{url:"arquivados", numero:1},
				{url:"configuracoes", numero:2}
			],
			tabAtiva: value
		}
		return(
			<div>
				<BottomNavigation
					className="navbar"
					value={value}
					onChange={(event, newValue) => {
						this.setState({value:newValue});
					}}
					showLabels
					>
					<BottomNavigationAction onClick={() => this.props.history.push(`${match.url}/listagem`)} label="Pacientes Ativos" icon={<FaceTwoTone />} />
					<BottomNavigationAction onClick={() => this.props.history.push(`${match.url}/arquivados`)} label="Pacientes Arquivados" icon={<ArchiveTwoTone />} />
					<BottomNavigationAction onClick={() => this.props.history.push(`${match.url}/config`)} label="Configurações" icon={<SettingsTwoTone />} />
				</BottomNavigation>
				<Switch>
					<Route path={`${match.url}/config`} render={props => <PacientesConfig {...props} tab={tab} trocarTab={this.trocarTab.bind(this)} />} />					
					<Route exact path={`${match.url}/listagem`} render={props => <PacientesListagem {...props} tab={tab} trocarTab={this.trocarTab.bind(this)}/>} />
					<Route exact path={`${match.url}/arquivados`} render={props => <PacientesArquivados {...props} tab={tab} trocarTab={this.trocarTab.bind(this)} />} />
					<Route exact path={`${match.url}/`} render={()=> <Redirect to={`${match.url}/listagem`} />} />
				</Switch>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		selectedPatient: state.patientsCreation.selectedPatient,
	};
}
export default connect(mapStateToProps)(PacientesRouter);