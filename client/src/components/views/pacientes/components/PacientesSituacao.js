	import React, { Component } from 'react';
	import { connect } from 'react-redux';

	import FormComponent from '../../../form/FormComponent';
	import Notificacao from '../../../notificacao/Notificacao';
	import {cadastrarSituacao, buscarSituacao, editarSituacao} from '../../../../api/pacientes'

	class PacientesSituacao extends Component {

		state = {
			formulario:[
				{type: 'text', name:'titulo', label:'Situação'}
			],
			alerta:{
				tipo:'',
				msg:'',
				abrir:false
			},
			situacao:'',
			cadastro:true,
			await:true
		}

		async componentWillMount(){
			const { clinica } = this.props;

			if(this.props.match.params.situacao_id){
				var objSituacao = await buscarSituacao(clinica._id, this.props.match.params.situacao_id);
				this.setState({cadastro:false, situacao:objSituacao, await:false})
			}else{
				this.setState({await:false});
			}
		}

		editaSituacao(objeto){
			editarSituacao( objeto )
			.then(r => {
				if(r){
					this.setState({
						alerta:{
							tipo:'success',
							msg:'Situacao editada com sucesso',
							abrir:true
						}
					});
					setTimeout(() => this.props.history.goBack(), 3000)
				}else{
					this.setState({
						alerta:{
							tipo:'error',
							msg:'Infelizmente não foi possível salvar a situação!',
							abrir:true
						}
					})
				}
			})
			.then(this.setState({alerta:{abrir:false}}))

		}

		salvarSituacao(objeto){
			const { clinica } = this.props;

			cadastrarSituacao( clinica, objeto.titulo)
			.then(r => {
				if(r){
					this.setState({
						alerta:{
							tipo:'success',
							msg:'Situacao cadastrada com sucesso',
							abrir:true
						}
					});
					setTimeout(() => this.props.history.goBack(), 3000)
				}else{
					this.setState({
						alerta:{
							tipo:'error',
							msg:'Infelizmente não foi possível salvar a situação!',
							abrir:true
						}
					})
				}
			})
			.then(this.setState({alerta:{abrir:false}}))
		}

		render(){
			return(
				<div>
					{this.state.alerta.abrir &&
						<Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg}/>
					}
					{!this.state.await &&
						<FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.situacao} action={this.state.cadastro?this.salvarSituacao.bind(this):this.editaSituacao.bind(this)} />
					}
				</div>
			);
		}
	}

	function mapStateToProps(state) {
		
		const { auth } = state;

		return {
			user: auth.user,
			clinica: auth.clinic
		};
	}
	export default connect(mapStateToProps)(PacientesSituacao);