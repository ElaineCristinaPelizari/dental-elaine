import React, { Component } from 'react';
import { connect } from 'react-redux';

import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import {cadastrarStatus, editarStatus, buscarStatus} from '../../../../api/pacientes'

class PacientesStatus extends Component {

	state = {
		formulario:[
			{type: 'text', name:'status', label:'Status'}
		],
		alerta:{
			tipo:'',
			msg:'',
			abrir:false
		},
		status:'',
		cadastro:true,
		await:true
	}

	async componentWillMount(){
		const { clinica } = this.props;

		if(this.props.match.params.status_id){
			var objStatus = await buscarStatus(clinica._id, this.props.match.params.status_id);
			this.setState({cadastro:false, status:objStatus, await:false})
		}else{
			this.setState({await:false});
		}
	}

	editaStatus(objeto){
		editarStatus( objeto )
		.then(r => {
			if(r){
				this.setState({
					alerta:{
						tipo:'success',
						msg:'Status editado com sucesso',
						abrir:true
					}
				});
				setTimeout(() => this.props.history.goBack(), 1500)
			}else{
				this.setState({
					alerta:{
						tipo:'error',
						msg:'Infelizmente não foi possível salvar o status!',
						abrir:true
					}
				})
			}
		})
		.then(this.setState({alerta:{abrir:false}}))

	}

	salvarStatus(objeto){
		const { clinica } = this.props;

		cadastrarStatus( clinica, objeto.status)
			.then(r => {
				if(r){
					this.setState({
						alerta:{
							tipo:'success',
							msg:'Status cadastrado com sucesso',
							abrir:true
						}
					});
					setTimeout(() => this.props.history.goBack(), 1500)
				}else{
					this.setState({
						alerta:{
							tipo:'error',
							msg:'Infelizmente não foi possível salvar o status!',
							abrir:true
						}
					})
				}
			})
			.then(this.setState({alerta:{abrir:false}}))
	}

	render(){
		return(
			<div>
				{this.state.alerta.abrir &&
					<Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg}/>
				}
				{!this.state.await &&
					<FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.status} action={this.state.cadastro?this.salvarStatus.bind(this):this.editaStatus.bind(this)} />
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps)(PacientesStatus);