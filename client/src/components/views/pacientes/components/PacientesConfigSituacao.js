import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deletarSituacao, buscarSituacoes } from "../../../../api/pacientes";
import { editPatient } from '../../../../actions/patientsSearch';

import TableComponent from '../../../table/TableComponent';

//Icone de arquivar
import ArchiveTwoTone from '@material-ui/icons/ArchiveTwoTone';
import EditTwoTone from '@material-ui/icons/EditTwoTone';

class PacientesConfigSituacao extends Component {
	state = {
		situacoes:[]
	}

	deletaSituacao(objeto){
		const { clinica } = this.props;

		deletarSituacao(objeto, clinica._id);
	}
	 
	componentWillMount(){
        
		buscarSituacoes(this.props.clinica._id)
		.then(situacoes => {
			var listaSituacoes = []

			situacoes.length > 0 && situacoes.map( (situacao, index) => {
                var novaSituacao = {};
                
                novaSituacao.titulo = situacao.titulo;
                novaSituacao.situacaoId = situacao._id;
                novaSituacao.funcoes = [
                    <EditTwoTone key={index} onClick={() => this.props.history.push(`/pacientes/config/cadastrarSituacao/${situacao._id}`)} />
                ]

                listaSituacoes.push(novaSituacao);
			});

			this.setState({situacoes:listaSituacoes})
		})
	}

	render(){

	const 	titulo = "Situações",
			colunas = [
                {label:'Titulo', name:'titulo'},
                {label:'Id', name:'situacaoId'},
				{label: '', name: 'funcoes', options:{filter:false, sort:false}},
			],
			objetos = this.state.situacoes,
			inserir = {
				nome: "Cadastrar Situação",
				link:"cadastrarSituacao"
			}
		
		return(
            <TableComponent funcoes={{deletar:this.deletaSituacao.bind(this)}} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props}/>
		);
	}
}

function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps, { editPatient })(PacientesConfigSituacao);