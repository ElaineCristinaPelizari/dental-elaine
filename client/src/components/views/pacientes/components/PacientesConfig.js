import React, { Component, useEffect } from 'react';
import { Switch, Route, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import FormComponent from '../../../form/FormComponent';
import PacientesConfigSituacao from './PacientesConfigSituacao';
import PacientesConfigStatus from './PacientesConfigStatus';
import PacientesConfigStatusItem from './PacientesConfigStatusItem';
import PacientesSituacao from './PacientesSituacao';
import PacientesStatus from './PacientesStatus';
import PacientesStatusItem from './PacientesStatusItem';

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    paper:{
        minHeight:'83vh',
    },
}));
  

function PacientesConfig(props){
    
    useEffect(() => {
        const { tab, trocarTab } = props;

		const tabSelecionada = tab.lista.filter(tab => tab.url === "configuracoes")[0];

		if(tabSelecionada.numero !== tab.tabAtiva){
			trocarTab(tabSelecionada.numero);
        }
    })

    const classes = useStyles();
    const { match } = props;

    return (
        <Paper className={classes.paper}>
            {match.isExact &&
                <List component="nav" className={classes.root} aria-label="Configurações">
                    <ListItem onClick={() => props.history.push(`${match.url}/situacao`)} button>
                        <ListItemText primary="Cadastro de situações" />
                    </ListItem>
                    <Divider />
                    <ListItem button divider onClick={() => props.history.push(`${match.url}/status`)} button>
                        <ListItemText primary="Cadastro de status" />
                    </ListItem>
                    {/* <ListItem button>
                        <ListItemText primary="Trash" />
                    </ListItem>
                    <Divider light />
                    <ListItem button>
                        <ListItemText primary="Spam" />
                    </ListItem> */}
                </List>
            }
            <Switch>
                <Route exact path={`${match.url}/situacao`} render={props => <PacientesConfigSituacao {...props} /> }/>
                <Route exact path={`${match.url}/cadastrarSituacao/:situacao_id?`} render={props => <PacientesSituacao {...props} /> }/>
                <Route exact path={`${match.url}/status`} render={props => <PacientesConfigStatus {...props} /> }/>
                <Route exact path={`${match.url}/status/:status_id`} render={props => <PacientesConfigStatusItem {...props} /> }/>
                <Route exact path={`${match.url}/cadastrarStatus/:status_id?`} render={props => <PacientesStatus {...props} /> }/>
                <Route exact path={`${match.url}/cadastrarStatusItem/:statusItem_id?`} render={props => <PacientesStatusItem {...props} /> }/>
            </Switch>
        </Paper>
    );
}


function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps)(PacientesConfig);