import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { buscarPacientes } from "../../../../api/pacientes";
import { editPatient } from '../../../../actions/patientsSearch';

import TableComponent from '../../../table/TableComponent';
//Icone de arquivar
import ArchiveTwoTone from '@material-ui/icons/ArchiveTwoTone';
import EditTwoTone from '@material-ui/icons/EditTwoTone';


class PacientesListagem extends Component {
	state = {
		pacientes:[]
	}
	 
	componentWillMount(){
        const { tab, trocarTab, editPatient } = this.props;

		const tabSelecionada = tab.lista.filter(tab => tab.url === "listagem")[0];

		if(tabSelecionada.numero !== tab.tabAtiva){
			trocarTab(tabSelecionada.numero);
        }
        
		buscarPacientes(this.props.clinica._id)
		.then(pacientes => {
			var listaPacientes = []

			pacientes.length > 0 && pacientes.map( paciente => {
                if(paciente.active){
					var novoPaciente = {};
					
					novoPaciente.registro = paciente.registry
                    novoPaciente.paciente = paciente.name;
                    novoPaciente.email = paciente.email || "";
                    novoPaciente.telefone = paciente.telephones.filter(telefone => telefone.name === "Principal")[0].value || "";
                    novoPaciente.funcoes = [
						<Tooltip key={paciente.name} title={"Arquivar"}>
							<IconButton onClick={() => editPatient({id:paciente._id, message:`paciente ${paciente.name} arquivado com sucesso!`, props:{active: false}})}>
								<ArchiveTwoTone />
							</IconButton>
						</Tooltip>,
						<Tooltip key={paciente.registry} title={"Editar"}>
							<IconButton onClick={() => this.props.history.push(`/patients/profile/${paciente._id}`)} >
								<EditTwoTone />
							</IconButton>
						</Tooltip>,
					]

                    listaPacientes.push(novoPaciente);    
                }
			});

			this.setState({pacientes:listaPacientes})	
		})
	}

	render(){

	const 	titulo = "Pacientes Ativos",
			colunas = [
				{label:'Registro', name:'registro'},
				{label: 'Paciente', name: 'paciente'},
				{label: 'Email', name: 'email'},
				{label: 'Telefone', name: 'telefone'},
				{label: '', name: 'funcoes', options:{filter:false, sort:false}},
			],
			objetos = this.state.pacientes
		
		return(
            <TableComponent titulo={titulo} colunas={colunas} objetos={objetos} {...this.props}/>
		);
	}
}

function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps, { editPatient })(PacientesListagem);