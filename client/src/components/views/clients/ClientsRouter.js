import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import ClientesListagem from './components/ClientesListagem';

class ClientsRouter extends Component {

	render(){
		
		const { match } = this.props;
		
		return(
			<Switch>
				<Route exact path={`${match.url}/listagem`} render={props => <ClientesListagem {...this.props} />} />
				<Route exact path={`${match.url}/`} render={()=> <Redirect to={`${match.url}/listagem`} />} />
			</Switch>
		);
	}
}

function mapStateToProps(state) {
	return {
		selectedPatient: state.patientsCreation.selectedPatient,
	};
}
export default connect(mapStateToProps)(ClientsRouter);