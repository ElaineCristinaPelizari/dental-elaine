import React, { Component } from 'react';
import { connect } from 'react-redux';
import { buscarClientes } from "../../../../api/clientes";

import TableComponent from '../../../table/TableComponent';

class ClientesListagem extends Component {
	state = {
		clientes:[]
	}
	 
	componentWillMount(){
		buscarClientes()
		.then(clientes => {
			var listaClientes = []

			clientes.map( cliente => {
				var novoCliente = {};

				novoCliente.cliente = cliente.name;

				listaClientes.push(novoCliente);
			});

			this.setState({clientes:listaClientes})	
		})
	}

	render(){

	const 	titulo = "Clientes",
			colunas = [
				// {title: 'Cliente', field: 'cliente'}
				{label: 'Cliente', name: 'cliente'}
			],
			objetos = this.state.clientes

		if(this.state.clientes.length === 0) return null;
		
		return(
			<TableComponent titulo={titulo} colunas={colunas} objetos={objetos} {...this.props}/>
		);
	}
}

function mapStateToProps(state) {
	return {
		selectedPatient: state.patientsCreation.selectedPatient,
	};
}
export default connect(mapStateToProps)(ClientesListagem);