import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select"
import { listarTabelas } from "../../../../api/servicos";
// import { editPatient } from '../../../../actions/patientsSearch';

import TableComponent from '../../../table/TableComponent';
//Icone de arquivar
import ArchiveTwoTone from '@material-ui/icons/ArchiveTwoTone';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import { MenuItem, FormControl, InputLabel } from '@material-ui/core';


class ServicosListagem extends Component {
	state = {
		listaTabelas:[],
		listIdTabelas:[],
		tabelaSelecionada:'',
	}
	 
	componentWillMount(){
        const { tab, trocarTab } = this.props;

		const tabSelecionada = tab.lista.filter(tab => tab.url === "listagem")[0];

		if(tabSelecionada.numero !== tab.tabAtiva){
			trocarTab(tabSelecionada.numero);
        }
        
		listarTabelas(this.props.clinica._id)
		.then(tabelas => {
			var idTabelas = [];

			tabelas.map(tabela => {
				if(tabela.ativo === true){
					idTabelas.push({_id:tabela._id, titulo:tabela.titulo, ativo:tabela.ativo} )
				}
			})
			this.setState({listaTabelas:tabelas, listIdTabelas:idTabelas, tabelaSelecionada:idTabelas[0]._id})
		})
	}

	selecionarTabela = event =>{
		this.setState({tabelaSelecionada:event.target.value})
	}

	render(){

		var tabela = this.state.listaTabelas.filter((tabela)=>{return tabela._id === this.state.tabelaSelecionada})[0];
		
		var objeto = [];

		tabela && tabela.servicos.map(servico =>{
			var novoServico = {};
					
			novoServico.descricao = servico.descricao
			novoServico.preco = servico.preco;
			novoServico.precoFinal = (servico.preco * tabela.indice).toFixed(2);
			objeto.push(novoServico);
		})

		const titulo = "Tabela de serviço",
			colunas = [
				{label:'Descrição', name:'descricao'},
				{label: 'Preço base', name: 'preco'},
				{label: 'Preço final', name: 'precoFinal'}
			],
			objetos = tabela && objeto
		
		return(
			<Paper>
				<FormControl style={{margin:"20px 5px"}}>
					<Select
						value={this.state.tabelaSelecionada}
						onChange={this.selecionarTabela}
					>
						{this.state.listIdTabelas.filter(tabela=>{return tabela.ativo === true}).map(tabela =>(<MenuItem value={tabela._id}>{tabela.titulo}</MenuItem>))}
					</Select>
				</FormControl>
            	<TableComponent selecionarLinhas={"none"} titulo={titulo} colunas={colunas} objetos={objetos} {...this.props}/>
			</Paper>
		);
	}
}

function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps)(ServicosListagem);