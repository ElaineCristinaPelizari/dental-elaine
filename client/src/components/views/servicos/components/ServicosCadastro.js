import React, { Component } from 'react';
import { connect } from 'react-redux';

import FormComponent from '../../../form/FormComponent';
import Notificacao from '../../../notificacao/Notificacao';
import { cadastrarServico, buscarServico, editarServico } from '../../../../api/servicos'

class ServicosCadastro extends Component {

	state = {
		formulario:[
            {type: 'text', name:'descricao', label:'Descrição'},
            {type: 'number', name:'preco', label:'Preço'},
            {type: 'select', name:'ativo', label:'Status', values:[{name:"Ativo", value:true }, {name:"Inativo",value:false}]},            
		],
		alerta:{
			tipo:'',
			msg:'',
			abrir:false
		},
		servico:'',
		cadastro:true,
		await:true
	}

	async componentWillMount(){
		const { clinica } = this.props;

		if(this.props.match.params.servico_id){
			var objServico = await buscarServico(clinica._id, this.props.match.params.servico_id);
			this.setState({cadastro:false, servico:objServico, await:false})
		}else{
			this.setState({await:false});
		}
	}

	editaServico(objeto){
		editarServico( objeto )
		.then(r => {
			if(r){
				this.setState({
					alerta:{
						tipo:'success',
						msg:'Serviço editado com sucesso!',
						abrir:true
					}
				});
				setTimeout(() => this.props.history.goBack(), 1500)
			}else{
				this.setState({
					alerta:{
						tipo:'error',
						msg:'Infelizmente não foi possível salvar o serviço!',
						abrir:true
					}
				})
			}
		})
		.then(this.setState({alerta:{abrir:false}}))

	}

	salvarServico(objeto){
		const { clinica } = this.props;

		cadastrarServico( clinica, objeto)
			.then(r => {
				if(r){
					this.setState({
						alerta:{
							tipo:'success',
							msg:'Servico cadastrado com sucesso',
							abrir:true
						}
					});
					setTimeout(() => this.props.history.goBack(), 1500)
				}else{
					this.setState({
						alerta:{
							tipo:'error',
							msg:'Infelizmente não foi possível salvar o serviço!',
							abrir:true
						}
					})
				}
			})
			.then(this.setState({alerta:{abrir:false}}))
	}

	render(){
		return(
			<div>
				{this.state.alerta.abrir &&
					<Notificacao tipo={this.state.alerta.tipo} msg={this.state.alerta.msg}/>
				}
				{!this.state.await &&
					<FormComponent {...this.props} inputs={this.state.formulario} objeto={this.state.servico} action={this.state.cadastro?this.salvarServico.bind(this):this.editaServico.bind(this)} />
                }
			</div>
		);
	}
}

function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps)(ServicosCadastro);