import React, { Component } from 'react';
import { connect } from 'react-redux';
import { listarServicos, deletarServico } from "../../../../api/servicos";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

import TableComponent from '../../../table/TableComponent';

//Icone de arquivar
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import PageviewTwoTone from '@material-ui/icons/PageviewTwoTone'

class ServicosConfigServico extends Component {
	state = {
		servicos:[]
	}

	deletaServico(objeto){
		const { clinica } = this.props;
		deletarServico(objeto, clinica._id);
	}
	 
	componentWillMount(){
        
		listarServicos(this.props.clinica._id)
		.then(arrServicos => {
			var listaServicos = []

			arrServicos.length > 0 && arrServicos.map( (servico, index) => {
                var novoServico = {};
                
                novoServico.descricao = servico.descricao;
				novoServico.preco = servico.preco.toFixed(2);
				novoServico.ativo = servico.ativo?"Ativo":"Inativo";
				novoServico._id = servico._id;
                novoServico.funcoes = [
					<Tooltip title={"Editar Serviço"} key={index}  >
						<IconButton onClick={() => this.props.history.push(`/servicos/config/cadastrarServico/${servico._id}`)}>
							<EditTwoTone />
						</IconButton>
					</Tooltip>
                ]

                listaServicos.push(novoServico);
			});

			this.setState({servicos:listaServicos})
		})
		.catch(erro => console.log(erro))
	}

	render(){

	const 	titulo = "Servico",
			colunas = [
                {label:'Serviço', name:'descricao'},
                {label:'Preço base', name:'preco'},
                {label:'Ativo', name:'ativo'},
				{label: '', name: 'funcoes', options:{filter:false, sort:false}},
			],
			objetos = this.state.servicos,
			inserir = {
				nome: "Cadastrar Serviço",
				link:"cadastrarServico"
			}
		
		return(
            <TableComponent funcoes={{deletar:this.deletaServico.bind(this)}} titulo={titulo} inserir={inserir} colunas={colunas} objetos={objetos} {...this.props}/>
		);
	}
}

function mapStateToProps(state) {
    
    const { auth } = state;

	return {
        user: auth.user,
        clinica: auth.clinic
	};
}
export default connect(mapStateToProps)(ServicosConfigServico);