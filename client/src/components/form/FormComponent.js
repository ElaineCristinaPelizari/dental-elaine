import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import { CirclePicker } from 'react-color';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { Select } from '@material-ui/core';
import { buscarCep } from '../../api/Cep';

export default class FormComponent extends Component {

    constructor(props) {
        super(props);
        this.state = !props.objeto ? this.criarState(this.props.inputs) : this.props.objeto;
    }

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };

    handleChangeColor = (e, name) => {
        this.setState({ [name]: e.hex });
    };

    criarState(arr) {
        var state = {};
        arr.map(i => {
            state[i.name] = ''
        })

        return state;
    }

    renderizarAcao(action) {
        var button = '';

        button = <Button onClick={() => action(this.state)} variant="contained" color="secondary">Salvar</Button>;

        return button;
    }


    handleDados(cep) {
        if (cep.target.value.length == 8) {
            const c = cep.target.value;
            buscarCep(c).then((res) => {
                console.log(res);
                this.setState({
                    objeto: {
                        logradouro: res.logradouro,
                        bairro: res.bairro,
                        cidade: res.cidade,
                        uf: res.estado
                    },
                })
            })
        } else {
            return null;
        }
    }

    renderizar(objeto) {
        var input = '';

        switch (objeto.type) {
            case 'cep':
                input =
                    <div>
                        <InputLabel htmlFor={objeto.name}>{objeto.label}</InputLabel>
                        <Input type="number" name="cep" id="cep" value={this.state[objeto.name]} onBlur={(cep) => this.handleDados(cep)} style={{ width: '50%' }} aria-describedby={objeto.label} id={objeto.name} />
                    </div>
                break;
            case 'text':
                input =
                    <div>
                        <InputLabel htmlFor={objeto.name}>{objeto.label}</InputLabel>
                        <Input value={this.state[objeto.name]} onChange={this.handleChange(objeto.name)} style={{ width: '50%' }} aria-describedby={objeto.label} id={objeto.name} />
                    </div>
                break;  
            case 'number':
                input =
                    <div>
                        <InputLabel htmlFor={objeto.name}>{objeto.label}</InputLabel>
                        <Input type="number" value={this.state[objeto.name]} onChange={this.handleChange(objeto.name)} style={{ width: '50%' }} aria-describedby={objeto.label} id={objeto.name} />
                    </div>
                break;
            case 'select':
                input =
                    <div>
                        <FormControl style={{ width: "50%" }}>
                            <InputLabel htmlFor="age-native-simple">{objeto.label}</InputLabel>
                            <Select value={this.state[objeto.name]} onChange={this.handleChange(objeto.name)}
                                inputProps={{ id: 'age-native-simple', }}>
                                <option value={0}>Selecione ----</option>
                                {
                                    objeto.values.map((v, i) => <option key={i} value={v.value}>{v.name}</option>)
                                }
                            </Select>
                        </FormControl>
                    </div>
                break;
            case 'color':
                input =
                    <div>
                        <InputLabel style={{ display: 'block', margin: '7px 0', transform: 'none', position: 'relative' }}>{objeto.label}</InputLabel>
                        <div>
                            <CirclePicker color={this.state[objeto.name]} onChangeComplete={(e) => this.handleChangeColor(e, objeto.name)} />
                        </div>
                    </div>
        }

        return input;
    }

    componentWillReceiveProps(props) {

        for (var objeto in props.objeto) {
            this.setState({ [objeto]: props.objeto[objeto] });
        }
    }

    render() {
        const { inputs, action } = this.props;
        return (
            <Container>
                {
                    inputs.map((input, key) => (
                        <FormControl fullWidth key={key}>
                            {
                                this.renderizar(input, key)
                            }
                        </FormControl>
                    ))
                }
                <FormControl style={{ display: 'block', marginTop: "30px" }}>
                    <Button style={{ marginRight: '20px' }} onClick={() => this.props.history.goBack()} variant="contained" color="primary">voltar</Button>
                    {
                        action &&
                        this.renderizarAcao(action)
                    }
                </FormControl>
            </Container>
        );
    }
}