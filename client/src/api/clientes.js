import axios from 'axios';

export async function buscarClientes(){
    var clinicas = await axios.get('/api/listClinics')
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return clinicas;
}