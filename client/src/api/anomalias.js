import axios from 'axios';

//Listar anomalias
export async function buscarAnomalias(clinicaId) {
    var anomalias = await axios.post('/api/anomalias/listar',
        {
            "clinic_id": clinicaId
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anomalias;
}

//Cadastrar anomalias
export async function cadastrarAnomalia(clinicaId, objeto) {
    var anomalia = await axios.post('/api/anomalias/cadastrarAnomalia',
        {
            "clinic_id": clinicaId._id,
            "codigo": objeto.codigo,
            "descricao": objeto.descricao,
        })
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anomalia;
}

// Buscar Anomalia 
export async function buscarAnomalia(clinicaId, anomaliaId) {
    var anomalia = await axios.post('/api/anomalias/buscarAnomalia',
        {
            "anomalia_id": anomaliaId,
            "clinic_id": clinicaId
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anomalia;
}

// Editar anomalias
export async function editarAnomalia(objAnomalia) {
    var anomalia = await axios.put('/api/anomalias/editarAnomalia',
        {
            objAnomalia
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anomalia;
}

// Excluir anomalia
export async function deletarAnomalia(objAnomalia, clinic_id) {
    objAnomalia.clinic_id = clinic_id;

    var anomalia = await axios.delete('/api/anomalias/deletarAnomalia',
        {
            data: objAnomalia
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anomalia;
}