import axios from 'axios';

export async function buscarAnamneses(clinicaId) {
    var anamneses = await axios.post('/api/anamnese/listar',
        {
            "clinic_id": clinicaId
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anamneses;
}

export async function cadastrarAnamnese(clinicaId, objeto) {
    console.log(objeto);
    var anamnese = await axios.post('/api/anamnese/cadastrarAnamnese',
        {
            "clinic_id": clinicaId._id,
            "nomeFicha": objeto.nomeFicha,
            "data": objeto.data,
            "perguntas": objeto.perguntas,
        })
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anamnese;
}

export async function buscarFichaPergunta( clinic_id, anamnese_id ){
    console.log(anamnese_id);
    var status = await axios.post('/api/anamnese/buscarFichaPergunta',
        {
            "clinic_id":clinic_id,
            "anamnese_id":anamnese_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

export async function buscarAnamnese(clinicaId, anamneseId) {
    var anamnese = await axios.post('/api/anamnese/buscarAnamnese',
        {
            "anamnese_id": anamneseId,
            "clinic_id": clinicaId
        }
    )
        .then(response => response.status === 200 && response.dataques)
        .catch(error => console.log(error))

    return anamnese;
}

export async function editarAnamnese(objAnamnese) {
    console.log(objAnamnese);
    var anamnese = await axios.put('/api/anamnese/editarAnamnese',
        {
            objAnamnese
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anamnese;
}

export async function deletarAnamnese(objAnamnese, clinic_id) {
    objAnamnese.clinic_id = clinic_id;

    var anamnese = await axios.delete('/api/anamnese/deletarAnamnese',
        {
            data: objAnamnese
        }
    )
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return anamnese;
}