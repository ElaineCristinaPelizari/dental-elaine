import axios from 'axios';

// Listar tabelas
export async function listarTabelas( clinic_id ){
    var status = await axios.post('/api/servico/listarTabelas',
        {
            "clinic_id": clinic_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Listar servicos
export async function listarServicos( clinic_id ){
    var status = await axios.post('/api/servico/listarServicos',
        {
            "clinic_id": clinic_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Cadastrar tabela
export async function cadastrarTabela( clinic_id, objeto ){
    var status = await axios.post('/api/servico/cadastrarTabela',
        {
            "clinic_id":clinic_id,
            "titulo":objeto.titulo,
            "indice":objeto.indice,
            "ativo":objeto.ativo
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Cadastrar servico
export async function cadastrarServico( clinic_id, objeto ){
    var status = await axios.post('/api/servico/cadastrarServico',
        {
            "clinic_id":clinic_id,
            "descricao":objeto.descricao,
            "preco":objeto.preco,
            "ativo":objeto.ativo
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Editar serviço
export async function editarServico( objServico ){
    var status = await axios.put('/api/servico/editarServico',
        {
            objServico
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Editar tabela
export async function editarTabela( objTabela ){
    var status = await axios.put('/api/servico/editarTabela',
        {
            objTabela
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Buscar tabela
export async function buscarTabela( clinic_id, tabela_id ){
    var status = await axios.post('/api/servico/buscarTabela',
        {
            "clinic_id":clinic_id,
            "tabela_id":tabela_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Buscar servico
export async function buscarServico( clinic_id, servico_id ){
    var status = await axios.post('/api/servico/buscarServico',
        {
            "clinic_id":clinic_id,
            "servico_id":servico_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Excluir servico
export async function deletarServico( objServico, clinic_id ){
    objServico.clinic_id = clinic_id;

    var status = await axios.delete('/api/servico/deletarServico',
        {
            data:objServico
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Excluir tabela
export async function deletarTabela( objTabela, clinic_id ){
    objTabela.clinic_id = clinic_id;

    var status = await axios.delete('/api/servico/deletarTabela',
        {
            data:objTabela
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Desvincular servico da tabela
export async function desvincularServico( tabela_id, objServico ){
    var objRemove = {};
    var arrServico = [];

    objRemove.tabela_id = tabela_id;
    
    objServico.map(servico => {
        arrServico.push(servico[0]._id);
    });

    objRemove.servicos = arrServico;

    var status = await axios.post('/api/servico/removerServicoTabela',
        objRemove
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Vincular servico na tabela
export async function adicionarServicoTabela( tabela_id, objServico ){
    console.log(objServico)
    var status = await axios.post('/api/servico/adicionarServicoTabela',
        {
            tabela_id: tabela_id,
            servicos: [objServico.servicos]
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}