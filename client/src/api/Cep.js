import axios from 'axios';

export async function buscarCep(cep) {
    var cep = await axios.get(`http://api.postmon.com.br/v1/cep/${cep}`)
        .then(response => response.status === 200 && response.data)
        .catch(error => console.log(error))

    return cep;
}