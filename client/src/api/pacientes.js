import axios from 'axios';

// Listar pacientes
export async function buscarPacientes(clinicaId){
    var pacientes = await axios.post('/api/pacientes/listar',
        {
            "clinic_id":clinicaId
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return pacientes;
}

// Listar situacoes
export async function buscarSituacoes(clinicaId){
    var situacoes = await axios.post('/api/pacientes/listarSituacao',
        {
            "clinic_id":clinicaId
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return situacoes;
}

// Cadastrar situacoes
export async function cadastrarSituacao(clinicaId, situacao){
    var situacao = await axios.post('/api/pacientes/cadastrarSituacao',
    {
        "clinic_id":clinicaId,
        "titulo":situacao
    })
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return situacao;
}

// Buscar situacao 
export async function buscarSituacao( clinicaId, situacaoId ){
    var situacao = await axios.post('/api/pacientes/buscarSituacao',
        {
            "situacao_id": situacaoId,
            "clinic_id": clinicaId
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return situacao;
}

// Editar situacao
export async function editarSituacao( objSituacao ){
    var situacao = await axios.put('/api/pacientes/editarSituacao',
        {
            objSituacao
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return situacao;
}

// Excluir situacao
export async function deletarSituacao( objSituacao, clinic_id ){
    objSituacao.clinic_id = clinic_id;

    var situacao = await axios.delete('/api/pacientes/deletarSituacao',
        {
            data:objSituacao
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return situacao;
}

// Listar status 
export async function listarStatus(clinicaId){
    var status = await axios.post('/api/pacientes/listarStatus',
        {
            "clinic_id":clinicaId
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Listar todos os status e itens 
export async function listarTodoStatus(clinicaId){
    var status = await axios.post('/api/pacientes/listarTodoStatus',
        {
            "clinic_id":clinicaId
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Listar itens do status
export async function listarStatusItem(status_id){
    var status = await axios.post('/api/pacientes/listarStatusItem',
        {
            "status_id": status_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Cadastrar status
export async function cadastrarStatus(clinicaId, status){
    var status = await axios.post('/api/pacientes/cadastrarStatus',
    {
        "clinic_id":clinicaId,
        "status":status
    })
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Cadastrar item status
export async function cadastrarStatusItem(status_id, item){
    var status = await axios.post('/api/pacientes/cadastrarStatusItem',
    {
        "status_id": status_id,
        "titulo": item
    })
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Buscar status 
export async function buscarStatus( clinic_id, status_id ){
    var status = await axios.post('/api/pacientes/buscarStatus',
        {
            "status_id": status_id,
            "clinic_id": clinic_id
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Buscar itens do status 
export async function buscarStatusItem( statusItem_id ){
    var status = await axios.post('/api/pacientes/buscarStatusItem',
        {
            "statusItem_id": statusItem_id,
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Editar status
export async function editarStatus( objStatus ){
    var status = await axios.put('/api/pacientes/editarStatus',
        {
            objStatus
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Editar status
export async function editarStatusItem( objStatusItem ){
    var status = await axios.put('/api/pacientes/editarStatusItem',
        {
            objStatusItem
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Excluir status
export async function deletarStatus( objStatus, clinic_id ){
    objStatus.clinic_id = clinic_id;

    var status = await axios.delete('/api/pacientes/deletarStatus',
        {
            data:objStatus
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}

// Excluir itens do status
export async function deletarStatusItem( objStatusItem){

    var status = await axios.delete('/api/pacientes/deletarStatusItem',
        {
            data:objStatusItem
        }
    )
    .then(response => response.status === 200 && response.data)
    .catch(error => console.log(error))

    return status;
}