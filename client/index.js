const express = require('express');
const proxy = require('http-proxy-middleware');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.use(proxy("/api",{ target:'http://localhost:5000' }));
app.use(proxy("/socket",{ target:'http://localhost:3030' }));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

var porta = process.env.PORT || 3000;

app.listen(porta);
  