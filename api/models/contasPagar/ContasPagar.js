const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContasPagarSchema = new Schema({
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'clinic',
        required: true
    },
    nome: {
        type: String
    },
    nota: {
        type: String
    },
    duplicada: {
        type: String
    },
    dataVenc: {
        type: String,
        required: true
    },
    indice: {
        type: Number
    },
    valor: {
        type: Number,
        required: true
    },
    nPrestacao: {
        type: Number
    },
    totalPrestacao: {
        type: Number
    },
    observacao: {
        type: String
    },
    descricao:
    {
        type: Schema.Types.ObjectId,
        ref: 'Descricao'
    },
    contasPagas:
    [{
        type: Schema.Types.ObjectId,
        ref: 'contasPagas'
    }],

});

const ContasPagar = mongoose.model('contasPagar', ContasPagarSchema);
module.exports = ContasPagar;