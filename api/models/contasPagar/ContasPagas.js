const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContasPagasSchema = new Schema({
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'clinic',
        required: true
    },
    valorPago: {
        type: Number
    },
    dataPagamento: {
        type: String
    },
    observacao: {
        type: String
    },
    contasPagar: 
    {
        type: Schema.Types.ObjectId,
        ref: 'contasPagar',
        required: true
    }
});

const ContasPagas = mongoose.model('contasPagas', ContasPagasSchema);
module.exports = ContasPagas;