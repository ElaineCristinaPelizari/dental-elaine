const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DescricaoSchema = new Schema({
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'clinic',
        required: true
    },
    codigo: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        required: true
    }
});

const Descricao = mongoose.model('descricao', DescricaoSchema);
module.exports = Descricao;