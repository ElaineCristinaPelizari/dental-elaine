const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const convenioSchema = new Schema({
    codigo: {
        type:String,
        required:true
    },
    nomeConvenio: {
        type:String,
        required:true
    },
    endereco: {
        type:String
    },
    cidade: {
        type:String
    },
    uf: {
        type:String
    },
    cep: {
        type:Number
    },
    telefone: {
        type:String
    },
    cnpj: {
        type:Number
    },
    inscEstadual: {
        type:Number
    },
    email: {
        type:String
    },
    site: {
        type:String
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Convenio = mongoose.model('convenio', convenioSchema);
module.exports = Convenio;