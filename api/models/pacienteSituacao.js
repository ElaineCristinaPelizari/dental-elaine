const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const situacaoSchema = new Schema({
    titulo: {
        type:String,
        required:true
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Situacao = mongoose.model('situacao', situacaoSchema);
module.exports = Situacao;