const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const fornecedorSchema = new Schema({
    codigo: {
        type:String,
        required:true
    },
    nome: {
        type:String,
        required:true
    },
    telefone: {
        type:String
    },
    contato: {
        type:String,
        required:true
    },
    cnpj: {
        type:Number
    },
    email: {
        type:String
    },
    site: {
        type:String
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Fornecedor = mongoose.model('fornecedor', fornecedorSchema);
module.exports = Fornecedor;