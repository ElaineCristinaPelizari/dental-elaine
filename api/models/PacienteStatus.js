const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const statusSchema = new Schema({
    status: {
        type:String,
        required:true
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Status = mongoose.model('status', statusSchema);
module.exports = Status;