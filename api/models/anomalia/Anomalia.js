const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const anomaliaSchema = new Schema({
    codigo: {
        type:String,
        required:true
    },
    descricao: {
        type:String,
        required:true
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Anomalia = mongoose.model('anomalia', anomaliaSchema);
module.exports = Anomalia;