const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const statusItemSchema = new Schema({
    titulo: {
        type:String,
        required:true
    },
    status_id:{
        type: Schema.Types.ObjectId,
        ref: 'status',
        required:true
    }
});

const StatusItem = mongoose.model('statusItem', statusItemSchema);
module.exports = StatusItem;