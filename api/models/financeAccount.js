const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const financeAccountSchema = new Schema({
  name: String,
  balance: Number,
  bills: [{
    document: String,
    credor: String,
    category: String,
    paymentMethod: String,
    description: String,
    maturity: Date,
    amount: Number,
    repeat: {
      will: Boolean,
      interval: {
        qtd: Number,
        rule: String
      },
      infinity: {
        will: Boolean,
        portion: Number,
        paidPortion: Number
      }
    }
  }],
  payments: [{
    credor: String,       // Credor
    code: String,         // Código do Documento
    maturity: Date,       // Data de vencimento
    debt: Number,         // Débito
    description: Number,  // Descrição
    atSight: Boolean      // Pagamento irá se repetir
  }],
  active: Boolean
});

const FinanceAccount = mongoose.model('financeAccount', financeAccountSchema);
module.exports = FinanceAccount;