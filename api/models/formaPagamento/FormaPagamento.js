const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const formaPagamentoSchema = new Schema({
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
    codigo: {
        type:String,
        required:true
    },
    tipo: {
        type:String,
        required:true
    }
});

const FormaPagamento = mongoose.model('formaPagamento', formaPagamentoSchema);
module.exports = FormaPagamento;