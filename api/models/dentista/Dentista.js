const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dentistaSchema = new Schema({
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'clinic',
        required: true
    },
    codigo: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    logradouro: {
        type: String
    },
    cidade: {
        type: String
    },
    uf: {
        type: String
    },
    bairro: {
        type: String
    },
    cep: {
        type: Number
    },
    cpf: {
        type: String,
        required: true
    },
    rg: {
        type: String
    },
    cro: {
        type: Number,
        required: true
    },
    partic: {
        type: Number
    },
    birthday: {
        type: String
    },
    telephone: {
        type: Number,
        required: true
    },
   telResidencial: {
        type: Number
    }, 
    telComercial: {
        type: Number
    },
    telFax: {
        type: Number
    },
    email: {
        type: String
    },
    especialidades: [
        {
            type: Schema.Types.ObjectId,
            ref: 'DentistaEspecialidade'
        }
    ],
    categorias: [
        {
            type: Schema.Types.ObjectId,
            ref: 'DentistaCategoria'
        }
    ],

});

const Dentista = mongoose.model('dentista', dentistaSchema);
module.exports = Dentista;