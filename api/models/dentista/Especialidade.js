const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const especialidadeSchema = new Schema({
    titulo: {
        type:String,
        required:true
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Especialidade = mongoose.model('especialidade', especialidadeSchema);
module.exports = Especialidade;