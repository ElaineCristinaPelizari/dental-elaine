const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const categoriaSchema = new Schema({
    titulo: {
        type:String,
        required:true
    },
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
});

const Categoria = mongoose.model('categoria', categoriaSchema);
module.exports = Categoria;