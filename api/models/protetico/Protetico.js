const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const proteticoSchema = new Schema({
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'clinic',
        required: true
    },
    codigo: {
        type: String
    },
    nome: {
        type: String
    },
    adress: {
        type: String
    },
    city: {
        type: String
    },
    uf: {
        type: String
    },
    cep: {
        type: Number
    },
    cpf: {
        type: String
    },
    rg: {
        type: String
    },
    cnpj: {
        type: Number
    },
    inscEstadual: {
        type: Number
    },
    telefone: {
        type: Number
    },
    email: {
        type: String
    },
    site: {
        type: String
    },
    tabelaPrecos: [
        {
            type: Schema.Types.ObjectId,
            ref: 'TabelaPreco'
        }
    ],
});

const Protetico = mongoose.model('protetico', proteticoSchema);
module.exports = Protetico;