const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tabelaPrecoSchema = new Schema({
    clinic_id: {
        type: Schema.Types.ObjectId,
        ref: 'clinic',
        required: true
    },
    codigo: {
        type: String
    },
    descricao: {
        type: String
    },
    valorIndice: {
        type: Number
    },
    valor: {
        type: Number
    }
});

const tabelaPreco = mongoose.model('tabelaPreco', tabelaPrecoSchema);
module.exports = tabelaPreco;