const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const anamneseSchema = new Schema({
	clinic_id: {
		type: Schema.Types.ObjectId,
		ref: 'clinic',
		required: true
	},
	nomeFicha: String,
	data: Date,
	perguntas: [{
		tip: String,
		pergunta: String,
		tamanho: Number,
		opcao: String,
	}],
});

const Anamnese = mongoose.model('Anamnese', anamneseSchema);
module.exports = Anamnese;
