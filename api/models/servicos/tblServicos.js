const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const tblServicoSchema = new Schema({
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
    descricao:{
        type: String,
        required: true
    },
    preco:{
        type: Number,
        required: true
    },
    observacao:{
        type: String
    },
    cor:{
        type: String,
    },
    ativo:{
        type: Boolean,
        default: true
    }

});

const tblServico = mongoose.model('tblServico', tblServicoSchema);
module.exports = tblServico;