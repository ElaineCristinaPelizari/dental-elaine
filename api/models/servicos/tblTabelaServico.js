const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const tblTabelaServicoSchema = new Schema({
    clinic_id: {
	    type: Schema.Types.ObjectId,
        ref: 'clinic',
        required:true
	},
    titulo:{
        type: String,
        required: true
    },
    indice:{
        type: Number,
        required: true
    },
    servicos:[
        {
            type: Schema.Types.ObjectId,
            ref: 'tblServico'
        }
    ],
    ativo:{
        type: Boolean,
        default: true
    }

});

const tblTabelaServico = mongoose.model('tblTabelaServico', tblTabelaServicoSchema);
module.exports = tblTabelaServico;