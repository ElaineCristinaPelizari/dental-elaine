const _ = require('lodash'); 
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Situacao = require('../models/pacienteSituacao');

module.exports = {
	criar(req, res, next){
        let props = req.body;
        
        Situacao.create(props)
        .then(situacao => {
            res.send(situacao)
        })
        .catch(next)
	},
	editar(req, res, next){
        const props = req.body.objSituacao;
		Situacao.findById({ _id: props._id })
			.then(situacao => {
                if(situacao.clinic_id == props.clinic_id){
                    situacao.update(props)
                        .then(() => Situacao.findById({ _id: props._id }))
                        .then(situacao => {
                            situacao.save();
                            res.send(situacao);
                        })
                        .catch(next);
                }else{
                    res.sendStatus(401)
                }
			})
			.catch(next);
    },
    buscar(req,res,next){
        const props = req.body;

        Situacao.findById({ _id: props.situacao_id})
            .then(situacao => {
                if(situacao.clinic_id == props.clinic_id){
                    res.send(situacao);
                }else{
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
	deletar(req, res, next){
        const props = req.body;
        
        props.map(elemento => {
            if(props.clinic_id === elemento[0].clinic_id){

                Situacao.findById({ _id: elemento[0].situacaoId })
                    .then(situacao => {
                        situacao.remove(situacao)
                            .then(() => Situacao.findById({ _id: elemento[0].situacaoId }))
                            .then(situacao => {
                                res.send(situacao);
                            });
                    })
                    .catch(next);
            }else{
                res.sendStatus(401);
            }
        })

    },
    listar(req, res, next){
        const props = req.body;

        Situacao.find({clinic_id: ObjectId(props.clinic_id)})
            .then(situacoes => {
                res.send(situacoes);
            })
            .catch(next);
    }
};