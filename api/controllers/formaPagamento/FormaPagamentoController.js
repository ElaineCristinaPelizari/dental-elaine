const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const FormaPagamento = require('../../models/formaPagamento/FormaPagamento');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        FormaPagamento.create(props)
            .then(formaPagamento => {
                res.send(formaPagamento)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objFormaPagamento;
        console.log(props);
        FormaPagamento.findById({ _id: props._id })
            .then(formaPagamento => {
                if (formaPagamento.clinic_id == props.clinic_id) {
                    formaPagamento.update(props)
                        .then(() => FormaPagamento.findById({ _id: props._id }))
                        .then(formaPagamento => {
                            formaPagamento.save();
                            res.send(formaPagamento);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        console.log(props);
        FormaPagamento.findById({ _id: props.formaPagamento_id })
            .then(formaPagamento => {
                if (formaPagamento.clinic_id == props.clinic_id) {
                    res.send(formaPagamento);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {
                FormaPagamento.findById({ _id: elemento[0].formaPagamentoId })
                    .then(formaPagamento => {
                        formaPagamento.remove(formaPagamento)
                            .then(() => FormaPagamento.findById({ _id: elemento[0].formaPagamentoId }))
                            .then(formaPagamento => {
                                res.send(formaPagamento);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })

    },
    listar(req, res, next) {
        const props = req.body;
        FormaPagamento.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(formaPagamentos => {
                res.send(formaPagamentos);
            })
            .catch(next);
    }
};