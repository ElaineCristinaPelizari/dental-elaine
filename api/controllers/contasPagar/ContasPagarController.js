const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Conta = require('../../models/contasPagar/ContasPagar');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        Conta.create(props)
            .then(conta => {
                res.send(conta)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objConta;
        Conta.findById({ _id: props._id })
            .then(conta => {
                if (conta.clinic_id == props.clinic_id) {
                    conta.update(props)
                        .then(() => Conta.findById({ _id: props._id }))
                        .then(conta => {
                            conta.save();
                            res.send(conta);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Conta.findById({ _id: props.conta_id })
            .then(conta => {
                if (conta.clinic_id == props.clinic_id) {
                    res.send(conta);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {
                Conta.findById({ _id: elemento[0].contaId })
                    .then(conta => {
                        conta.remove(conta)
                            .then(() => Conta.findById({ _id: elemento[0].contaId }))
                            .then(conta => {
                                res.send(conta);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        Conta.find({ clinic_id: ObjectId(props.clinic_id) })
            .populate({
                path: "descricao",
                model: "descricao"
            })
            .populate({
                path: "contasPagas",
                model: "contasPagas"
            })
            .then(contas => {
                res.send(contas);
            })
            .catch(next);
    },
};