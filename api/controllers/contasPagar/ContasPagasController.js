const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const ContaPaga = require('../../models/contasPagar/ContasPagas');
const ContaPagar = require('../../models/contasPagar/ContasPagar');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        ContaPaga.create(props)
            .then(contaPaga => {
                ContaPagar.findById({ _id: contaPaga.contasPagar })
                    .then(contaPagar => {
                        contaPagar.update({ $addToSet: { contasPagas: contaPaga._id } })
                            .then(conta => res.send(conta))
                            .catch(next);
                    })
                    .catch(next);
            })
    },
    editar(req, res, next) {
        const props = req.body.objContaPaga;
        ContaPaga.findById({ _id: props._id })
            .then(contaPaga => {
                if (contaPaga.clinic_id == props.clinic_id) {
                    contaPaga.update(props)
                        .then(() => ContaPaga.findById({ _id: props._id }))
                        .then(contaPaga => {
                            contaPaga.save();
                            res.send(contaPaga);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        ContaPaga.findById({ _id: props.contaPaga_id })
            .populate({
                path: "contasPagar",
                model: "contasPagar"
            })
            .then(contaPaga => {
                if (contaPaga.clinic_id == props.clinic_id) {
                    res.send(contaPaga);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {
                ContaPaga.findById({ _id: elemento[0].contaPagaId })
                    .then(contaPaga => {
                        ContaPagar.findById({ _id: contaPaga.contasPagar })
                            .then(contaPagar => {
                                contaPagar.update({ $pull: { contasPagas: contaPaga._id } })
                                    .then(conta)
                                    .catch(next);
                            })
                        ContaPaga.remove(contaPaga)
                            .then(() => ContaPaga.findById({ _id: elemento[0].contaPagaId }))
                            .then(contaPaga => {
                                res.send(contaPaga);
                            })
                            .catch(next);
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        ContaPaga.find({ clinic_id: ObjectId(props.clinic_id) })
            .populate({
                path: "contasPagar",
                model: "contasPagar"
            })
            .then(contasPagas => {
                res.send(contasPagas);
            })
            .catch(next);
    },
};