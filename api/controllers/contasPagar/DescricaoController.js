const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Descricao = require('../../models/contasPagar/Descricao');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        Descricao.create(props)
            .then(descricao => {
                res.send(descricao)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objDescricao;
        Descricao.findById({ _id: props._id })
            .then(descricao => {
                if (descricao.clinic_id == props.clinic_id) {
                    descricao.update(props)
                        .then(() => Descricao.findById({ _id: props._id }))
                        .then(descricao => {
                            descricao.save();
                            res.send(descricao);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Descricao.findById({ _id: props.descricao_id })
            .then(descricao => {
                if (descricao.clinic_id == props.clinic_id) {
                    res.send(descricao);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {
                Descricao.findById({ _id: elemento[0].descricaoId })
                    .then(descricao => {
                        descricao.remove(descricao)
                            .then(() => Descricao.findById({ _id: elemento[0].descricaoId }))
                            .then(descricao => {
                                res.send(descricao);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        Descricao.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(descricoes => {
                res.send(descricoes);
            })
            .catch(next);
    },
};