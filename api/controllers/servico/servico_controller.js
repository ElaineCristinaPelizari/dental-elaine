const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const TabelaServico = require('../../models/servicos/tblTabelaServico');
const Servico = require('../../models/servicos/tblServicos');

module.exports = {
	criarTabela(req, res, next){
        let props = req.body;
        
        TabelaServico.create(props)
        .then(tabela => {
            res.send(tabela);
        })
        .catch(next)
    },
    cadastrarServico(req, res, next){
        let props = req.body;

        Servico.create(props)
        .then(servico =>{
            res.send(servico)
        })
        .catch(next)
    },
    addServicoTabela(req, res, next){
        let props = req.body;

        if(props.tabela_id && props.servicos){

            
            TabelaServico.findById({ _id: props.tabela_id })
                .then(tabela => {

                    tabela.update({$addToSet:{servicos:{$each:props.servicos, $exists:false}}})
                    .then(() => TabelaServico.findById({ _id: props.tabela_id }))
                        .then(tabela => {
                            tabela.save();
                            res.send(tabela);
                        })
                        .catch(next);
                })

                .catch(next);
        }else{
            res.sendStatus(401);
        }
    },
    removerServicoTabela(req,res, next){
        let props = req.body;
        TabelaServico.findById({ _id: props.tabela_id })
            .then(tabela => {
                var servicos = tabela.servicos;
                
                var novos = "";

                for (let i = 0; i < props.servicos.length; i++) {
                    novos = servicos.filter(servico => {
                        if(servico){
                            return servico.toString() !== props.servicos[i]
                        }
                    });
                }
                
                tabela.update({$set:{servicos:novos}})
                .then(() => TabelaServico.findById({ _id: props.tabela_id }))
                    .then(tabela => {
                        tabela.save();
                        res.send(tabela);
                    })
                    .catch(next);
            })

            .catch(next);

    },
    listarTabelas(req,res,next){
        let props = req.body;

        TabelaServico.find({ clinic_id: ObjectId(props.clinic_id)})
        .populate({
            path:'servicos',
            model:'tblServico'
        })
        .then(tabela => {
            res.json(tabela);
        })
        .catch(next);
    },
    listarServicos(req,res, next){
        let props = req.body;

        Servico.find({clinic_id: ObjectId(props.clinic_id)})
            .then(servicos => {
                res.json(servicos);
            })
            .catch(next);
    },
    buscarTabela(req,res,next){
        let props = req.body;

        TabelaServico.findById({ _id: props.tabela_id})
        .populate({
            path:'servicos',
            model:'tblServico'
        })
        .then(tabela => {
            res.json(tabela);
        })
        .catch(next);
    },
    buscarServico(req,res,next){
        let props = req.body;

        Servico.findById({_id: props.servico_id})
            .then(servico => {
                res.json(servico);
            })
            .catch(next);
    },
    cadastrarItem(req,res,next){
        let props = req.body;

        StatusItem.create(props)
            .then(statusItem => {
                res.send(statusItem);
            })
            .catch(next);
    },
    editarServico(req,res,next){
        const props = req.body.objServico;

        Servico.findById({_id:props._id})
            .then(servico => {
                servico.update(props)
                .then(() => Servico.findById({_id:props._id}))
                .then(servico => {
                    servico.save();
                    res.send(servico);
                })
                .catch(next);
            })
    },
    editarTabela(req,res,next){
        const props = req.body.objTabela;

        TabelaServico.findById({_id:props._id})
            .then(tabela => {
                tabela.update(props)
                .then(() => TabelaServico.findById({_id:props._id}))
                .then(tabela => {
                    tabela.save();
                    res.send(tabela);
                })
                .catch(next);
            })
    },
	editar(req, res, next){
        const props = req.body.objStatus;
		Status.findById({ _id: props._id })
			.then(status => {
                if(status.clinic_id == props.clinic_id){
                    status.update(props)
                        .then(() => Status.findById({ _id: props._id }))
                        .then(status => {
                            status.save();
                            res.send(status);
                        })
                        .catch(next);
                }else{
                    res.sendStatus(401)
                }
			})
			.catch(next);
    },
    editarItem(req, res, next){
        
        const props = req.body.objStatusItem;

		StatusItem.findById({ _id: props._id })
			.then(item => {
                item.update(props)
                    .then(() => StatusItem.findById({ _id: props._id }))
                    .then(item => {
                        item.save();
                        res.send(item);
                    })
                    .catch(next);
			})
			.catch(next);
    },
    buscar(req,res,next){
        const props = req.body;

        Status.findById({ _id: props.status_id})
            .then(status => {
                if(status.clinic_id == props.clinic_id){
                    res.send(status);
                }else{
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    buscarItens(req,res,next){
        const props = req.body;

        StatusItem.findById({_id: props.statusItem_id})
            .then(item => {
                res.send(item)
            })
            .catch(next);
    },
	deletarTabela(req, res, next){
        const props = req.body;
        
        props.map(elemento => {
            if(props.clinic_id === elemento[0].clinic_id){

                TabelaServico.findById({ _id: elemento[0]._id })
                    .then(tabela => {
                        tabela.remove(tabela)
                            .then(() => TabelaServico.findById({ _id: elemento[0]._id }));
                    })
                    .catch(next);
            }else{
                res.sendStatus(401);
            }
        })

    },
    deletarServico(req, res, next){
        const props = req.body;
        props.map(elemento => {
            Servico.findById({ _id: elemento[0]._id })
                .then(servico => {
                    servico.remove(servico)
                        .then(() => Servico.findById({ _id: elemento[0]._id }))
                })
                .catch(next);
        })

    },
    listar(req, res, next){
        const props = req.body;

        Status.find({clinic_id: ObjectId(props.clinic_id)})
            .then(status => {
                res.send(status);
            })
            .catch(next);
    },
    listarStatusItens(req, res, next){
        const props = req.body;

        StatusItem.find({status_id: ObjectId(props.status_id)})
            .then(itens => {
                res.send(itens)
            })
            .catch(next)
    },
    async listarTodoStatus(req,res,next){
        const props = req.body;

        if(props.clinic_id.length <12)res.send([])

        var status = await Status.find({"clinic_id": ObjectId(props.clinic_id)});
        
        var statusItens = [];

        for(i = 0; i < status.length; i++){
            var itens = await StatusItem.find({status_id: ObjectId(status[i]._doc._id)});
            
            statusItens.push({
                status_id:status[i]._doc._id,
                status:status[i]._doc.status,
                itens:[]
            });

            for( x = 0; x < itens.length; x ++){
                
                var json = {
                    status_id: status[i]._doc._id,
                    status: status[i]._doc.status,
                    item_status_id: itens[x]._doc._id,
                    item_titulo: itens[x]._doc.titulo
                }
                statusItens[i].itens.push(json);
                
            }
            
        }

        res.send(statusItens);

    }

};