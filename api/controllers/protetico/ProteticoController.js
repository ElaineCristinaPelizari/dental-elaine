const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Protetico = require('../../models/protetico/Protetico');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        console.log(props)

        Protetico.create(props)
            .then(protetico => {
                res.send(protetico)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objProtetico;
        console.log(props)
        Protetico.findById({ _id: props._id })
            .then(protetico => {
                if (protetico.clinic_id == props.clinic_id) {
                    protetico.update(props)
                        .then(() => Protetico.findById({ _id: props._id }))
                        .then(protetico => {
                            protetico.save();
                            res.send(protetico);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Protetico.findById({ _id: props.protetico_id })
            .then(protetico => {
                if (protetico.clinic_id == props.clinic_id) {
                    res.send(protetico);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Protetico.findById({ _id: elemento[0].proteticoId })
                    .then(protetico => {
                        protetico.remove(protetico)
                            .then(() => Protetico.findById({ _id: elemento[0].proteticoId }))
                            .then(protetico => {
                                res.send(protetico);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        console.log(props);
        Protetico.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(proteticos => {
                res.send(proteticos);
            })
            .catch(next);
    },
    // Adiciona TabelaPreco em proteticos
    addTabelaPrecoProtetico(req, res, next) {
        let props = req.body;
        console.log(props);
        if (props.protetico_id && props.tabelaPrecos) {
            Protetico.findById({ _id: props.protetico_id })
                .then(protetico => {
                    protetico.update({ $addToSet: { tabelaPrecos: { $each: props.tabelaPrecos, $exists: false } } })
                        .then(() => Protetico.findById({ _id: props.protetico_id }))
                        .then(protetico => {
                            protetico.save();
                            res.send(protetico);
                        })
                        .catch(next);
                })
                .catch(next);
        } else {
            res.sendStatus(401);
        }
    },
    removerTabelaPrecoProtetico(req, res, next) {
        let props = req.body;
        Protetico.findById({ _id: props.protetico_id })
            .then(protetico => {
                var tabelaPrecos = protetico.tabelaPrecos;

                var novos = "";

                for (let i = 0; i < props.tabelaPrecos.length; i++) {
                    novos = tabelaPrecos.filter(tabelaPreco => {
                        if (tabelaPreco) {
                            return tabelaPreco.toString() !== props.tabelaPrecos[i]
                        }
                    });
                }

                protetico.update({ $set: { tabelaPrecos: novos } })
                    .then(() => Protetico.findById({ _id: props.protetico_id }))
                    .then(protetico => {
                        protetico.save();
                        res.send(protetico);
                    })
                    .catch(next);
            })
            .catch(next);
    },
    buscarTabelaPrecoProtetico(req,res,next){
        let props = req.body;

        Protetico.findById({ _id: props.protetico_id})
        .populate({
            path:'tabelaPrecos',
            model:'tabelaPreco'
        })
        .then(protetico => {
            res.json(protetico);
        })
        .catch(next);
    },
    listarTabelaPrecoProtetico(req,res, next){
        let props = req.body;

        Protetico.find({clinic_id: ObjectId(props.clinic_id)})
            .then(tabelaPrecos => {
                res.json(tabelaPrecos);
            })
            .catch(next);
    }
};