const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const TabelaPreco = require('../../models/protetico/TabelaPreco');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        console.log(props)

        TabelaPreco.create(props)
            .then(tabelaPreco => {
                res.send(tabelaPreco)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objTabelaPreco;
        console.log(props)
        TabelaPreco.findById({ _id: props._id })
            .then(tabelaPreco => {
                if (tabelaPreco.clinic_id == props.clinic_id) {
                    tabelaPreco.update(props)
                        .then(() => TabelaPreco.findById({ _id: props._id }))
                        .then(tabelaPreco => {
                            tabelaPreco.save();
                            res.send(tabelaPreco);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        TabelaPreco.findById({ _id: props.tabelaPreco_id })
            .then(tabelaPreco => {
                if (tabelaPreco.clinic_id == props.clinic_id) {
                    res.send(tabelaPreco);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                TabelaPreco.findById({ _id: elemento[0].tabelaPrecoId })
                    .then(tabelaPreco => {
                        TabelaPreco.remove(tabelaPreco)
                            .then(() => TabelaPreco.findById({ _id: elemento[0].tabelaPrecoId }))
                            .then(tabelaPreco => {
                                res.send(tabelaPreco);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        TabelaPreco.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(tabelaPrecos => {
                res.send(tabelaPrecos);
            })
            .catch(next);
    },
};