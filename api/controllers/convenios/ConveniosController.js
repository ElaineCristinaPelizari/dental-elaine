const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Convenio = require('../../models/convenios/Convenios');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        console.log(props);
        Convenio.create(props)
            .then(convenio => {
                res.send(convenio)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objConvenio;
        Convenio.findById({ _id: props._id })
            .then(convenio => {
                if (convenio.clinic_id == props.clinic_id) {
                    convenio.update(props)
                        .then(() => Convenio.findById({ _id: props._id }))
                        .then(convenio => {
                            convenio.save();
                            res.send(convenio);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Convenio.findById({ _id: props.convenio_id })
            .then(convenio => {
                if (convenio.clinic_id == props.clinic_id) {
                    res.send(convenio);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Convenio.findById({ _id: elemento[0].convenioId })
                    .then(convenio => {
                        convenio.remove(convenio)
                            .then(() => Convenio.findById({ _id: elemento[0].convenioId }))
                            .then(convenio => {
                                res.send(convenio);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;

        Convenio.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(convenios => {
                res.send(convenios);
            })
            .catch(next);
    }
};