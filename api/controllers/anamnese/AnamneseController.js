const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Anamnese = require('../../models/anamnese/Anamnese');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        console.log(props);
        Anamnese.create(props)
            .then(anamnese => {
                res.send(anamnese)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objAnamnese;
        console.log(props)
        Anamnese.findById({ _id: props._id })
            .then(anamnese => {
                if (anamnese.clinic_id == props.clinic_id) {
                    anamnese.update(props)
                        .then(() => Anamnese.findById({ _id: props._id }))
                        .then(anamnese => {
                            anamnese.save();
                            res.send(anamnese);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Anamnese.findById({ _id: props.anamnese_id })
            .then(anamnese => {
                if (anamnese.clinic_id == props.clinic_id) {
                    res.send(anamnese);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Anamnese.findById({ _id: elemento[0].anamneseId })
                    .then(anamnese => {
                        anamnese.remove(anamnese)
                            .then(() => Anamnese.findById({ _id: elemento[0].anamneseId }))
                            .then(anamnese => {
                                res.send(anamnese);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        console.log(props);
        Anamnese.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(anamnese => {
                res.send(anamnese);
            })
            .catch(next);
    },
    buscarFichaPergunta(req,res,next){
        let props = req.body;

        Anamnese.findById({ _id: props.anamnese_id})
        .populate({
            path:'perguntas',
            model:'anamnese'
        })
        .then(anamnese => {
            res.json(anamnese);
        })
        .catch(next);
    },
};