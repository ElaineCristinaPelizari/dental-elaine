const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Dentista = require('../../models/dentista/Dentista');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        console.log(props)

        Dentista.create(props)
            .then(dentista => {
                res.send(dentista)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objDentista;
        console.log(props)
        Dentista.findById({ _id: props._id })
            .then(dentista => {
                if (dentista.clinic_id == props.clinic_id) {
                    dentista.update(props)
                        .then(() => Dentista.findById({ _id: props._id }))
                        .then(dentista => {
                            dentista.save();
                            res.send(dentista);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Dentista.findById({ _id: props.dentista_id })
            .then(dentista => {
                if (dentista.clinic_id == props.clinic_id) {
                    res.send(dentista);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Dentista.findById({ _id: elemento[0].dentistaId })
                    .then(dentista => {
                        dentista.remove(dentista)
                            .then(() => Dentista.findById({ _id: elemento[0].dentistaId }))
                            .then(dentista => {
                                res.send(dentista);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;
        Dentista.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(dentistas => {
                res.send(dentistas);
            })
            .catch(next);
    },
    // Adiciona especialidade em dentista
    addEspecialidadeDentista(req, res, next) {
        let props = req.body;

        if (props.dentista_id && props.especialidades) {
            Dentista.findById({ _id: props.dentista_id })
                .then(dentista => {
                    dentista.update({ $addToSet: { especialidades: { $each: props.especialidades, $exists: false } } })
                        .then(() => Dentista.findById({ _id: props.dentista_id }))
                        .then(dentista => {
                            dentista.save();
                            res.send(dentista);
                        })
                        .catch(next);
                })
                .catch(next);
        } else {
            res.sendStatus(401);
        }
    },
    removerEspecialidadeDentista(req, res, next) {
        let props = req.body;
        Dentista.findById({ _id: props.dentista_id })
            .then(dentista => {
                var especialidades = dentista.especialidades;

                var novos = "";

                for (let i = 0; i < props.especialidades.length; i++) {
                    novos = especialidades.filter(especialidade => {
                        if (especialidade) {
                            return especialidade.toString() !== props.especialidades[i]
                        }
                    });
                }

                dentista.update({ $set: { especialidades: novos } })
                    .then(() => Dentista.findById({ _id: props.dentista_id }))
                    .then(dentista => {
                        dentista.save();
                        res.send(dentista);
                    })
                    .catch(next);
            })
            .catch(next);
    },

    //Adiciona categoria em dentista
    addCategoriaDentista(req, res, next) {
        let props = req.body;
        if (props.dentista_id && props.categorias) {
            Dentista.findById({ _id: props.dentista_id })
                .then(dentista => {
                    dentista.update({ $addToSet: { categorias: { $each: props.categorias, $exists: false } } })
                        .then(() => Dentista.findById({ _id: props.dentista_id }))
                        .then(dentista => {
                            dentista.save();
                            res.send(dentista);
                        })
                        .catch(next);
                })
                .catch(next);
        } else {
            res.sendStatus(401);
        }
    },
    removerCategoriaDentista(req, res, next) {
        let props = req.body;
        Dentista.findById({ _id: props.dentista_id })
            .then(dentista => {
                var categorias = dentista.categorias;

                var novos = "";

                for (let i = 0; i < props.categorias.length; i++) {
                    novos = categorias.filter(categoria => {
                        if (categoria) {
                            return categoria.toString() !== props.categorias[i]
                        }
                    });
                }

                dentista.update({ $set: { categorias: novos } })
                    .then(() => Dentista.findById({ _id: props.dentista_id }))
                    .then(dentista => {
                        dentista.save();
                        res.send(dentista);
                    })
                    .catch(next);
            })
            .catch(next);
    },
    buscarCategoriaDentista(req,res,next){
        let props = req.body;

        Dentista.findById({ _id: props.dentista_id})
        .populate({
            path:'categorias',
            model:'categoria'
        })
        .then(dentista => {
            res.json(dentista);
        })
        .catch(next);
    },
    listarCategoriaDentista(req,res, next){
        let props = req.body;

        Servico.find({clinic_id: ObjectId(props.clinic_id)})
            .then(categorias => {
                res.json(categorias);
            })
            .catch(next);
    },
    buscarEspecialidadeDentista(req,res,next){
        let props = req.body;

        Dentista.findById({ _id: props.dentista_id})
        .populate({
            path:'especialidades',
            model:'especialidade'
        })
        .then(dentista => {
            res.json(dentista);
        })
        .catch(next);
    }
};