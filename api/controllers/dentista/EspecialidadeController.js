const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Especialidade = require('../../models/dentista/Especialidade');

module.exports = {
    criar(req, res, next) {
        let props = req.body;

        Especialidade.create(props)
            .then(especialidade => {
                res.send(especialidade)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objEspecialidade;
        Especialidade.findById({ _id: props._id })
            .then(especialidade => {
                if (especialidade.clinic_id == props.clinic_id) {
                    especialidade.update(props)
                        .then(() => Especialidade.findById({ _id: props._id }))
                        .then(especialidade => {
                            especialidade.save();
                            res.send(especialidade);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;

        Especialidade.findById({ _id: props.especialidade_id })
            .then(especialidade => {
                if (especialidade.clinic_id == props.clinic_id) {
                    res.send(especialidade);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;

        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {
                Especialidade.findById({ _id: elemento[0].especialidadeId })
                    .then(especialidade => {
                        especialidade.remove(especialidade)
                            .then(() => Especialidade.findById({ _id: elemento[0].especialidadeId }))
                            .then(especialidade => {
                                res.send(especialidade);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })
    },
    listar(req, res, next) {
        const props = req.body;

        Especialidade.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(especialidades => {
                res.send(especialidades);
            })
            .catch(next);
    }
};