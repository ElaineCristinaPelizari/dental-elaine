const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Categoria = require('../../models/dentista/Categoria');

module.exports = {
    criar(req, res, next) {
        let props = req.body;

        Categoria.create(props)
            .then(categoria => {
                res.send(categoria)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objCategoria;
        Categoria.findById({ _id: props._id })
            .then(categoria => {
                if (categoria.clinic_id == props.clinic_id) {
                    categoria.update(props)
                        .then(() => Categoria.findById({ _id: props._id }))
                        .then(categoria => {
                            categoria.save();
                            res.send(categoria);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;

        Categoria.findById({ _id: props.categoria_id })
            .then(categoria => {
                if (categoria.clinic_id == props.clinic_id) {
                    res.send(categoria);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;

        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Categoria.findById({ _id: elemento[0].categoriaId })
                    .then(categoria => {
                        categoria.remove(categoria)
                            .then(() => Categoria.findById({ _id: elemento[0].categoriaId }))
                            .then(categoria => {
                                res.send(categoria);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })

    },
    listar(req, res, next) {
        const props = req.body;

        Categoria.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(categorias => {
                res.send(categorias);
            })
            .catch(next);
    }
};