const financeAccount = require('../models/financeAccount');
const Data = require('../services/connect');

const AuditController = require('../controllers/audit_controller');

module.exports = {
  createAccount(req, res, next){
		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);

		const props = {...req.body, active: true};

		financeAccountdb.create(props)
			.then(resFinanceAccount => res.send(resFinanceAccount))
			.catch(next);
  },
  createFinance (req, res, next) {
		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);

		financeAccountdb.findById({ _id: req.params.id })
			.then(resFinanceAccount => {
				resFinanceAccount.update({ $push: { [req.params.finance]: req.body }})
					.then(() => res.send(true))
					.catch(next);
			})
			.catch(next);
  },
  editAccount(req, res, next) {
		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);

		const accountId = req.params.id;
		const props = req.body;

		financeAccountdb.findById({ _id: accountId })
			.then(account => {
				account.update(props)
          .then(() => res.send({...account, ...props}))
					.catch(next);
			})
			.catch(next);
  },
  editFinance (req, res, next) {
		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);
    var set = {};
    Object.entries(req.body.update).map((item) => set[`${req.params.finance}.$.${item[0]}`] = item[1])
		financeAccountdb.update( { _id: req.params.id, [`${req.params.finance}.$._id`]: req.body.financeId }, { $set: set	} )
			.then(() => res.send(true))
			.catch(next);
	},
	deleteAccount(req, res, next){
		const accountId = req.params.id;

		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);

		financeAccountdb.findById({ _id: accountId })
			.then(account =>
				account.update({ active: false })
					.then(() => res.send({...account, active: false}))
			)
			.catch(next);
  },
  deleteFinance(req, res, next) {
		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);

		financeAccountdb.findById({ _id: req.params.id })
			.then(account => {
				account.update({ $pull: { [req.params.finance]: {_id: req.params.financeId} } })
					.then(() => res.send(true))
					.catch(next);
			})
			.catch(next);
	},
	get(req, res, next) {
		const db = Data.getDatabaseConnection(req.session.clinic.clinic_data);
    let financeAccountdb = db.model('financeAccount', financeAccount.financeAccountSchema);
    var finance;
    if (req.params.finance === 'general')
      finance = ['bills', 'payments'];
    else
      finance = [req.params.finance];
    financeAccountdb.find(req.params.id === 'all'?{}:{ _id: req.params.id })
      .then(filterAccounts => {
        var finances = [], both = req.body.since && req.body.to;
        filterAccounts.map((account) => {
          finance.map((fnnc) => {
            account[fnnc].map((item) => {
              if (item.repeat.infinite) {
                if (req.body.to >= item.maturity) {
                  if (both) {
                    if (req.body.since > item.maturity) {
                      var maturity = new Date(item.maturity);
                      switch (item.repeat.interval) {
                        case 'year':
                          var begin = req.body.since.getFullYear(), end = req.body.to.getFullYear(), year = maturity.getFullYear();
                          if (end - begin >= item.repeat.qtd) {
                            finances.push({...item, finance: fnnc});
                            return;
                          }
                          else {
                            var init = (begin - year),
                            diff = init - (init % item.repeat.qtd) + item.repeat.qtd,
                            nextDate = maturity;
                            nextDate.setFullYear(begin + diff);
                            if (nextDate <= req.body.to) {
                              finances.push({...item, finance: fnnc});
                              return;
                            }
                          }
                          break;
                        case 'month':
                          var begin = req.body.since.getMonth() + (req.body.since.getFullYear() - maturity.getFullYear())*12,
                          end = req.body.to.getMonth() + (req.body.to.getFullYear() - maturity.getFullYear())*12,
                          month = maturity.getMonth();
                          if (end - begin >= item.repeat.qtd) {
                            finances.push({...item, finance: fnnc});
                            return;
                          }
                          else {
                            var init = (begin - month),
                            diff = init - (init % item.repeat.qtd) + item.repeat.qtd,
                            nextDate = maturity;
                            nextDate.setFullYear(maturity.getFullYear() + parseInt((begin + diff)/12));
                            nextDate.setMonth((begin + diff) % 12);
                            if (nextDate <= req.body.to) {
                              finances.push({...item, finance: fnnc});
                              return;
                            }
                          }
                          break;
                        default:
                          var days = 1, oneDay = 24*60*60*1000, // hours*minutes*seconds*milliseconds
                          begin = Math.round((req.body.since.getTime() - maturity.getTime()) / oneDay),
                          end = Math.round((req.body.to.getTime() - maturity.getTime()) / oneDay);
                          if (item.repeat.interval === 'week')
                            days = 7;
                          if (end - begin >= item.repeat.qtd * days) {
                            finances.push({...item, finance: fnnc});
                            return;
                          }
                          else {
                            var init = item.repeat.qtd * days,
                            diff = begin - (begin % init) + init,
                            nextDate = maturity;
                            nextDate.setDate(maturity.getDate() + diff);
                            if (nextDate <= req.body.to) {
                              finances.push({...item, finance: fnnc});
                              return;
                            }
                          }
                          break;
                      }
                    }
                    else if (req.body.since <= item.maturity && item.maturity <= req.body.to)
                      finances.push({...item, finance: fnnc});
                  }
                  else {
                    finances.push({...item, finance: fnnc});
                    return;
                  }
                }
              }
              else if (req.body.since <= item.maturity && (both ? item.maturity <= req.body.to : true))
                finances.push({...item, finance: fnnc});
            })
          })
        });
        if (req.params.id === 'all')
          res.send({accounts: filterAccounts.map((item) => {return { _id: item._id, name: item.name, balance: item.balance }}), finances});
        else {
          financeAccountdb.find({}, {_id: 1, name: 1, balance: 1})
          .then(accounts => res.send({accounts, finances}));
        }
      })
    .catch(next);
	},
}