const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Fornecedor = require('../../models/fornecedor/Fornecedor');

module.exports = {
    criar(req, res, next) {
        let props = req.body;
        //console.log(props);
        Fornecedor.create(props)
            .then(fornecedor => {
                res.send(fornecedor)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objFornecedor;
        console.log(props);
        Fornecedor.findById({ _id: props._id })
            .then(fornecedor => {
                if (fornecedor.clinic_id == props.clinic_id) {
                    fornecedor.update(props)
                        .then(() => Fornecedor.findById({ _id: props._id }))
                        .then(fornecedor => {
                            fornecedor.save();
                            res.send(fornecedor);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;
        Fornecedor.findById({ _id: props.fornecedor_id })
            .then(fornecedor => {
                if (fornecedor.clinic_id == props.clinic_id) {
                    res.send(fornecedor);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;
        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Fornecedor.findById({ _id: elemento[0].fornecedorId })
                    .then(fornecedor => {
                        fornecedor.remove(fornecedor)
                            .then(() => Fornecedor.findById({ _id: elemento[0].fornecedorId }))
                            .then(fornecedor => {
                                res.send(fornecedor);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })

    },
    listar(req, res, next) {
        const props = req.body;
        Fornecedor.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(fornecedores => {
                res.send(fornecedores);
            })
            .catch(next);
    }
};