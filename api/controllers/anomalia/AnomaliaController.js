const _ = require('lodash');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Anomalia = require('../../models/anomalia/Anomalia');

module.exports = {
    criar(req, res, next) {
        let props = req.body;

        Anomalia.create(props)
            .then(anomalia => {
                res.send(anomalia)
            })
            .catch(next)
    },
    editar(req, res, next) {
        const props = req.body.objAnomalia;
        Anomalia.findById({ _id: props._id })
            .then(anomalia => {
                if (anomalia.clinic_id == props.clinic_id) {
                    anomalia.update(props)
                        .then(() => Anomalia.findById({ _id: props._id }))
                        .then(anomalia => {
                            anomalia.save();
                            res.send(anomalia);
                        })
                        .catch(next);
                } else {
                    res.sendStatus(401)
                }
            })
            .catch(next);
    },
    buscar(req, res, next) {
        const props = req.body;

        Anomalia.findById({ _id: props.anomalia_id })
            .then(anomalia => {
                if (anomalia.clinic_id == props.clinic_id) {
                    res.send(anomalia);
                } else {
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    deletar(req, res, next) {
        const props = req.body;

        props.map(elemento => {
            if (props.clinic_id === elemento[0].clinic_id) {

                Anomalia.findById({ _id: elemento[0].anomaliaId })
                    .then(anomalia => {
                        anomalia.remove(anomalia)
                            .then(() => Anomalia.findById({ _id: elemento[0].anomaliaId }))
                            .then(anomalia => {
                                res.send(anomalia);
                            });
                    })
                    .catch(next);
            } else {
                res.sendStatus(401);
            }
        })

    },
    listar(req, res, next) {
        const props = req.body;

        Anomalia.find({ clinic_id: ObjectId(props.clinic_id) })
            .then(anomalias => {
                res.send(anomalias);
            })
            .catch(next);
    }
};