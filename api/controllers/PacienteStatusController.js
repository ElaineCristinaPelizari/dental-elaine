const _ = require('lodash'); 
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const Status = require('../models/pacienteStatus');
const StatusItem = require('../models/PacienteStatusItem');

module.exports = {
	criar(req, res, next){
        let props = req.body;
        
        Status.create(props)
        .then(status => {
            res.send(status);
        })
        .catch(next)
    },
    cadastrarItem(req,res,next){
        let props = req.body;

        StatusItem.create(props)
            .then(statusItem => {
                res.send(statusItem);
            })
            .catch(next);
    },
	editar(req, res, next){
        const props = req.body.objStatus;
		Status.findById({ _id: props._id })
			.then(status => {
                if(status.clinic_id == props.clinic_id){
                    status.update(props)
                        .then(() => Status.findById({ _id: props._id }))
                        .then(status => {
                            status.save();
                            res.send(status);
                        })
                        .catch(next);
                }else{
                    res.sendStatus(401)
                }
			})
			.catch(next);
    },
    editarItem(req, res, next){
        
        const props = req.body.objStatusItem;

		StatusItem.findById({ _id: props._id })
			.then(item => {
                item.update(props)
                    .then(() => StatusItem.findById({ _id: props._id }))
                    .then(item => {
                        item.save();
                        res.send(item);
                    })
                    .catch(next);
			})
			.catch(next);
    },
    buscar(req,res,next){
        const props = req.body;

        Status.findById({ _id: props.status_id})
            .then(status => {
                if(status.clinic_id == props.clinic_id){
                    res.send(status);
                }else{
                    res.sendStatus(401);
                }
            })
            .catch(next);
    },
    buscarItens(req,res,next){
        const props = req.body;

        StatusItem.findById({_id: props.statusItem_id})
            .then(item => {
                res.send(item)
            })
            .catch(next);
    },
	deletar(req, res, next){
        const props = req.body;
        
        props.map(elemento => {
            if(props.clinic_id === elemento[0].clinic_id){

                Status.findById({ _id: elemento[0].statusId })
                    .then(status => {
                        status.remove(status)
                            .then(() => Status.findById({ _id: elemento[0].statusId }))
                            .then(status => {
                                res.send(status);
                            });
                    })
                    .catch(next);
            }else{
                res.sendStatus(401);
            }
        })

    },
    deletarItem(req, res, next){
        const props = req.body;
        
        props.map(elemento => {
            StatusItem.findById({ _id: elemento[0].statusItem_id })
                .then(status => {
                    status.remove(status)
                        .then(() => StatusItem.findById({ _id: elemento[0].statusItem_id }))
                        .then(status => {
                            res.send(status);
                        });
                })
                .catch(next);
        })

    },
    listar(req, res, next){
        const props = req.body;

        Status.find({clinic_id: ObjectId(props.clinic_id)})
            .then(status => {
                res.send(status);
            })
            .catch(next);
    },
    listarStatusItens(req, res, next){
        const props = req.body;

        StatusItem.find({status_id: ObjectId(props.status_id)})
            .then(itens => {
                res.send(itens)
            })
            .catch(next)
    },
    async listarTodoStatus(req,res,next){
        const props = req.body;

        if(props.clinic_id.length <12)res.send([])

        var status = await Status.find({"clinic_id": ObjectId(props.clinic_id)});
        
        var statusItens = [];

        for(i = 0; i < status.length; i++){
            var itens = await StatusItem.find({status_id: ObjectId(status[i]._doc._id)});
            
            statusItens.push({
                status_id:status[i]._doc._id,
                status:status[i]._doc.status,
                itens:[]
            });

            for( x = 0; x < itens.length; x ++){
                
                var json = {
                    status_id: status[i]._doc._id,
                    status: status[i]._doc.status,
                    item_status_id: itens[x]._doc._id,
                    item_titulo: itens[x]._doc.titulo
                }
                statusItens[i].itens.push(json);
                
            }
            
        }

        res.send(statusItens);

    }

};