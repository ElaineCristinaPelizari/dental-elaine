
//Controller contas a pagar
const ContasPagarController = require('../../controllers/contasPagar/ContasPagarController');
const DescricaoController = require('../../controllers/contasPagar/DescricaoController');
const ContasPagasController = require('../../controllers/contasPagar/ContasPagasController');

module.exports = app => {

    //Contas a Pagar
    app.post('/api/contas/cadastrarConta', ContasPagarController.criar);
    app.put('/api/contas/editarConta', ContasPagarController.editar);
    app.delete('/api/contas/deletarConta', ContasPagarController.deletar);
    app.post('/api/contas/listar', ContasPagarController.listar);
    app.post('/api/contas/buscarConta', ContasPagarController.buscar);

    //Descrição conta
    app.post('/api/contas/cadastrarDescricao', DescricaoController.criar);
    app.put('/api/contas/editarDescricao', DescricaoController.editar);
    app.delete('/api/contas/deletarDescricao', DescricaoController.deletar);
    app.post('/api/contas/listarDescricao', DescricaoController.listar);
    app.post('/api/contas/buscarDescricao', DescricaoController.buscar);

    //Contas a Pagas
    app.post('/api/contas/cadastrarContasPagas', ContasPagasController.criar);
    app.put('/api/contas/editarContasPagas', ContasPagasController.editar);
    app.delete('/api/contas/deletarContasPagas', ContasPagasController.deletar);
    app.post('/api/contas/listarContasPagas', ContasPagasController.listar);
    app.post('/api/contas/buscarContasPagas', ContasPagasController.buscar);
}