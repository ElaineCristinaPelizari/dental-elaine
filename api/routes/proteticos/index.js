//Controller de Proteticos e TabelaPreco
const ProteticoController = require('../../controllers/protetico/ProteticoController');
const TabelaPrecoController = require('../../controllers/protetico/TabelaPrecoController');

module.exports = app => {
    app.post('/api/proteticos/cadastrarProtetico', ProteticoController.criar);
    app.put('/api/proteticos/editarProtetico', ProteticoController.editar);
    app.delete('/api/proteticos/deletarProtetico', ProteticoController.deletar);
    app.post('/api/proteticos/listar', ProteticoController.listar);
    app.post('/api/proteticos/buscarProtetico', ProteticoController.buscar);

    // Adicionando e removendo TabelaPreco em proteticos
    app.post('/api/proteticos/adicionarTabelaPrecoProtetico', ProteticoController.addTabelaPrecoProtetico);
    app.post('/api/proteticos/removerTabelaPrecoProtetico', ProteticoController.removerTabelaPrecoProtetico);
    app.post('/api/proteticos/buscarTabelaPrecoProtetico', ProteticoController.buscarTabelaPrecoProtetico);
    app.post('/api/proteticos/listarTabelaPrecoProtetico', ProteticoController.listarTabelaPrecoProtetico);

    //TabelaPreco
    app.post('/api/proteticos/cadastrarTabelaPreco', TabelaPrecoController.criar);
    app.put('/api/proteticos/editarTabelaPreco', TabelaPrecoController.editar);
    app.delete('/api/proteticos/deletarTabelaPreco', TabelaPrecoController.deletar);
    app.post('/api/proteticos/listarTabelaPreco', TabelaPrecoController.listar);
    app.post('/api/proteticos/buscarTabelaPreco', TabelaPrecoController.buscar);
}