
const DentistaController = require('../../controllers/dentista/DentistaController');

const CategoriaController = require('../../controllers/dentista/CategoriaController');

const EspecialidadeController = require('../../controllers/dentista/EspecialidadeController');

module.exports = app => {
    // Dentista
    app.post('/api/dentistas/cadastrarDentista', DentistaController.criar);
    app.put('/api/dentistas/editarDentista', DentistaController.editar);
    app.delete('/api/dentistas/deletarDentista', DentistaController.deletar);
    app.post('/api/dentistas/listar', DentistaController.listar);
    app.post('/api/dentistas/buscarDentista', DentistaController.buscar);

    // Adicionando e removendo categorias em dentista
    app.post('/api/dentistas/adicionarCategoriaDentista', DentistaController.addCategoriaDentista);
    app.post('/api/dentistas/removerCategoriaDentista', DentistaController.removerCategoriaDentista);
    app.post('/api/dentistas/buscarCategoriaDentista', DentistaController.buscarCategoriaDentista);
    app.post('/api/dentistas/listarCategoriaDentista', DentistaController.listarCategoriaDentista);

    //Adicionando e removendo especialidades em dentista
    app.post('/api/dentistas/adicionarEspecialidadeDentista', DentistaController.addEspecialidadeDentista);
    app.post('/api/dentistas/removerEspecialidadeDentista', DentistaController.removerEspecialidadeDentista);
    app.post('/api/dentistas/buscarEspecialidadeDentista', DentistaController.buscarEspecialidadeDentista);

    // Categorias
    app.post('/api/dentistas/cadastrarCategoria', CategoriaController.criar);
    app.put('/api/dentistas/editarCategoria', CategoriaController.editar);
    app.delete('/api/dentistas/deletarCategoria', CategoriaController.deletar);
    app.post('/api/dentistas/listarCategoria', CategoriaController.listar);
    app.post('/api/dentistas/buscarCategoria', CategoriaController.buscar);

    // Especialidades
    app.post('/api/dentistas/cadastrarEspecialidade', EspecialidadeController.criar);
    app.put('/api/dentistas/editarEspecialidade', EspecialidadeController.editar);
    app.delete('/api/dentistas/deletarEspecialidade', EspecialidadeController.deletar);
    app.post('/api/dentistas/listarEspecialidade', EspecialidadeController.listar);
    app.post('/api/dentistas/buscarEspecialidade', EspecialidadeController.buscar);
}