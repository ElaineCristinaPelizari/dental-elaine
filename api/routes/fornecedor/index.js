
const FornecedorController = require('../../controllers/fornecedor/FornecedorController');

module.exports = app => {
    // Dentista
    app.post('/api/fornecedor/cadastrarFornecedor', FornecedorController.criar);
    app.put('/api/fornecedor/editarFornecedor', FornecedorController.editar);
    app.delete('/api/fornecedor/deletarFornecedor', FornecedorController.deletar);
    app.post('/api/fornecedor/listar', FornecedorController.listar);
    app.post('/api/fornecedor/buscarFornecedor', FornecedorController.buscar);
}