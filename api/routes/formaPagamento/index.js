
const FormaPagamentoController = require('../../controllers/formaPagamento/FormaPagamentoController');

module.exports = app => {
    // tipos de pagamento
    app.post('/api/formaPagamento/cadastrarFormaPagamento', FormaPagamentoController.criar);
    app.put('/api/formaPagamento/editarFormaPagamento', FormaPagamentoController.editar);
    app.delete('/api/formaPagamento/deletarFormaPagamento', FormaPagamentoController.deletar);
    app.post('/api/formaPagamento/listar', FormaPagamentoController.listar);
    app.post('/api/formaPagamento/buscarFormaPagamento', FormaPagamentoController.buscar);
}