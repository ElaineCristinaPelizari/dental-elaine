//Controller convenios
const ConveniosController = require('../../controllers/convenios/ConveniosController');

module.exports = app => {

    //Convenios
    app.post('/api/convenios/cadastrarConvenio', ConveniosController.criar);
    app.put('/api/convenios/editarConvenio', ConveniosController.editar);
    app.delete('/api/convenios/deletarConvenio', ConveniosController.deletar);
    app.post('/api/convenios/listar', ConveniosController.listar);
    app.post('/api/convenios/buscarConvenio', ConveniosController.buscar);

}