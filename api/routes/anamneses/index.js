
const AnamneseController = require('../../controllers/anamnese/AnamneseController');

module.exports = app => {
	app.post('/api/anamnese/cadastrarAnamnese', AnamneseController.criar);
	app.put('/api/anamnese/editarAnamnese', AnamneseController.editar);
	app.delete('/api/anamnese/deletarAnamnese', AnamneseController.deletar);
	app.post('/api/anamnese/listar', AnamneseController.listar);
	app.post('/api/anamnese/buscarAnamnese', AnamneseController.buscar);
	app.post('/api/anamnese/buscarFichaPergunta', AnamneseController.buscarFichaPergunta)

}