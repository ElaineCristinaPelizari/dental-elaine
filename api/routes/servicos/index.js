// Controller de situacoes
const ServicoController = require('../../controllers/servico/servico_controller');

module.exports = app => {
    
	app.post('/api/servico/cadastrarTabela', ServicoController.criarTabela);
    app.post('/api/servico/cadastrarServico', ServicoController.cadastrarServico);
    app.post('/api/servico/adicionarServicoTabela', ServicoController.addServicoTabela);
    app.post('/api/servico/removerServicoTabela', ServicoController.removerServicoTabela);
    app.delete('/api/servico/deletarServico', ServicoController.deletarServico);
    app.delete('/api/servico/deletarTabela', ServicoController.deletarTabela);
    app.post('/api/servico/buscarTabela', ServicoController.buscarTabela);
    app.post('/api/servico/buscarServico', ServicoController.buscarServico);
    app.post('/api/servico/listarTabelas', ServicoController.listarTabelas);
    app.post('/api/servico/listarServicos', ServicoController.listarServicos);
    app.put('/api/servico/editarServico', ServicoController.editarServico);
    app.put('/api/servico/editarTabela', ServicoController.editarTabela);

};
