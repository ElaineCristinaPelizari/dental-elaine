const mongoose = require('mongoose');
const connections = {};

exports.getDatabaseConnection = function(dbName) {
    if(connections[dbName]) {
        //database connection already exist. Return connection object
        return connections[dbName];
    } else {
        // connections[dbName] = mongoose.createConnection('mongodb://admdentalqi:appdentalqi@ec2-18-208-148-42.compute-1.amazonaws.com:27017/dentalQI_?authSource=admin&w=1'+ dbName,{   
        connections[dbName] = mongoose.createConnection('mongodb://127.0.0.1:27017/dentalQI_?authSource=admin&w=1'+ dbName,{   
        useMongoClient: true,
            poolSize: 100,
            socketTimeoutMS: 999999,
            keepAlive: true,
            connectTimeoutMS: 999999 
        });

        return connections[dbName];
    }
}