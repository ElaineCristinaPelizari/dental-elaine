const app = require('./api/app');
process.env.NODE_ENV = 'production';
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log('Listening on port: ' + PORT);
});